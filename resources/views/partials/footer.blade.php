@php
$phone = \App\Contact::where('key', 'phone')->first()->value;
$address = \App\Contact::where('key', 'address')->first()->value;
$whatsapp = \App\Contact::where('key', 'whatsapp')->first()->value;
@endphp
<!-- Кнопка связи -->
<div class="whatsap-wrap_btn">
    <a class="whatsapp_btn" href="https://wa.me/{{ $whatsapp }}">
        <p class="whatsapp-btn_pulse" >
            <img src="{{ asset('assets/img/whatsapp.svg') }}"> <span>Написать на WhatsApp</span>
		</p>
    </a>
</div>
{{-- <div class="follow-me_instagram">
    <div class="follow-me_instagram-content">
        <a class="follow-me_instagram-close"><img src="{{ asset('assets/img/close.svg') }}"></a>
        <p>Подпишись на наш Instagram <a href="">@ms_krepezh</a> страницу, лайкни наш пост получи бонус</p>
    </div>
</div> --}}
<div class="fast-order">
    <div class="fast-order-form">
        <button class="close-fast-order"><img src="{{ asset('assets/img/close.svg') }}"></button>
        <h1>Быстрый заказ</h1>
        <form action="/send" method="post">
            @csrf
            <label>Ваше имя</label>
            <input type="text" name="name" placeholder="Введите Ваше имя">
            <label>Ваш телефон</label>
            <input type="tel" name="phone" placeholder="+7 " class="phone-mask">
            <label>Cообщение</label>
            <textarea  name="comment"></textarea>
            <button>Отправить</button>
        </form>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="footer-about">
                    <img src="{{ asset('assets/img/logo2.svg') }}">
                    <p>TOO “MS крепеж” работает с 2012 года по продаже крепежных и строительных товаров. Идейные соображения высшего порядка, а также реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации новых предложений.</p>
                    <span>© Copyright MS крепеж</span>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="footer-nav">
                    <div class="footer-links">
                        <a href="/">Главная </a>
                        <a href="/about_us">О компании </a>
                        
                        <a href="/delivery">Доставка </a>
                        <a href="/contact">Контакты </a>
                    </div>
                    <div class="footer-links">
                        <a href="/terms">Договор оферты</a>
                        <a href="/how_order">Как заказать?   </a>
                        <a href="/checkout">Оформить заказ </a>
                        <a href="/home">Корзина</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="footer-magazine">
                    <h1>Магазины</h1>
                    <div class="footer-magazine-wrap">
                        <div class="footer-magazine-col">
                            <h2>г. Алматы</h2>
                            <p>пр. Рыскулова 94
                                <br> +7 (727) 3751919
                            </p>
                            <h2>г. Актобе</h2>
                            <p>ул. Бекетай 1/1
                                <br> +7 (727) 3751919
                            </p>
                            <h2>г. Караганда</h2>
                            <p>пр. Рыскулова 94
                                <br> +7 (727) 3751919
                            </p>
                        </div>
                        <div class="footer-magazine-col">
                            <h2>г. Нурсултан</h2>
                            <p>пр. Рыскулова 94
                                <br> +7 (727) 3751919
                            </p>
                            <h2>г. Атырау</h2>
                            <p>ул. Бекетай 1/1
                                <br> +7 (727) 3751919
                            </p>
                            <h2>г. Петропавлск</h2>
                            <p>пр. Рыскулова 94
                                <br> +7 (727) 3751919
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="footer-contact">
                    <h1>{{ $phone }}</h1>
                    <a href="tel:{{ $phone }}"> Оставить заявку</a>
                    <h2>{!! $address !!}</h2>
                    <h6>Мы в соц. сетях</h6>
                    <a href="{{ $instagram }}"> <img src="{{ asset('assets/img/instagram.svg') }}"></a>
                </div>
            </div>
			<div class="col-md-12">
	<div class="nidge">
		<a href="https://nidge.kz" target="_blank"> Создание Сайтов.</a> Разработано В <a href="https://nidge.kz" target="_blank">NIDGE Digital Agency</a>
	</div>
</div>
        </div>
    </div>
</footer>