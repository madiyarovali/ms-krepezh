@foreach(ShoppingCart::all() as $item)
@php
$item_model = Product::find($item->id);
if ($item_model->options->count() > 0) {
    $option = \App\ProductOption::find($item->option_id);
    if ($item->service) {
        $item_model = Service::find($item->id);
    }
}
@endphp
<li>
    <div class="order-product-info">
        <div class="img">
            <img src="{{ asset("storage/$item_model->images") }}">
        </div>
        <div class="content" style="max-width: 200px;">
            <a href="/product/{{ $item_model->id }}">{{ $item_model->name }}</a>
            @if (isset($option))
            <p>{{ $option->option }}</p>
            @endif
            <p>Артикул: {{ $item_model->article }}</p>
        </div>
    </div>
    <div class="popular-product__btn-number">
        <span>Кол-во:</span>
        <input type="button" value="-" id="moins" class="minus-product control-btns" data-row-id="{{ $item->__raw_id }}">
        <input type="text" size="25" value="{{ $item->qty }}" class="count-product">
        <input type="button" value="+" id="plus" class="plus-product control-btns" data-row-id="{{ $item->__raw_id }}">
    </div>
    <div class="order-box_price">
        <span> {{ $item->qty }} шт x <b>{{ $item->price }}</b> </span>
        <h3 class="total">{{ $item->total }}</h3>
    </div>
    <button class="box-products-list_close checkout-btn" title="Удалить" data-row="{{ $item->__raw_id }}"><img src="{{ asset('assets/img/trash.svg') }}"></button>
</li>
@endforeach