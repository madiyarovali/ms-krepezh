@php
if (!isset($recommendations) or $recommendations == null) {
    $recommendations = \App\Product::orderBy('created_at', 'DESC')->limit(6)->get();
}
@endphp
<div class="row">
    <div class="col-md-12">
        <div class="recommend-title">
            <h1>{{ setting('site.featured_text') }}</h1>
        </div>
    </div>
    <div class="col-md-12">
        <div id="recommend-slider" class="owl-carousel-banner owl-theme owl-carousel owl-carousel-recommend">
            @include('partials.products', ['products' => $recommendations, 'hide_row' => true])
        </div>
    </div>
</div>