<div class="row">
	<div class="col-md-6 col-xs-12">
		<div class="services-block first" style="background: url({{ asset('storage/'. setting('site.designer_image'))  }})">
			<div class="content">
				<h1>{{ setting('site.designer_title') }}</h1>
				{!! setting('site.designer_text') !!}
				<a class="btn" href="/designers">Подробнее</a>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12">
		<div class="services-block second" style="background: url({{ asset('storage/'. setting('site.team_image'))  }})">
			<div class="content">
				<h1>{{ setting('site.team_title') }}</h1>
				{!! setting('site.team_text') !!}
				<a class="btn" href="/teams">Подробнее</a>
			</div>
		</div>
	</div>
</div>