<div class="row">
    <div class="col-md-4 col-xs-12">
        <div class="about-info_item">
            {!! setting('site.block_delivery') !!}
        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <div class="about-info_item">
            {!! setting('site.loan_block') !!}
        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <div class="about-info_item">
            {!! setting('site.reviews_block') !!}
        </div>
    </div>
</div>