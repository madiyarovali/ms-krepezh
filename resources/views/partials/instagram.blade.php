@php
$instagram = \App\Contact::where('key', 'instagram')->first()->value;
$facebook = \App\Contact::where('key', 'facebook')->first()->value;
$odnoklassniki = \App\Contact::where('key', 'odnoklassniki')->first()->value;
@endphp
<section class="instagram">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="instagram-link">
                    <h1>Наш Instagram @ms_krepezh</h1>
                    <a href="{{ $instagram }}">Подписаться</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-full">
        @foreach($posts as $post)
        <div class="instagram-img">
            <img src="{{ $post['image'] }}">
        </div>
        @endforeach
    </div>
</section>