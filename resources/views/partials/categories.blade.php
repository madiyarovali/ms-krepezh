@php
if (!isset($category)) {
    $categories = \App\Category::where('parent_id', null)->orderBy('order', 'ASC')->get();
}
@endphp
<div class="categories">
    <h1>Категории</h1>

    <div class="cd-dropdown-wrapper">
        <a class="cd-dropdown-trigger" href="#0">Категории</a>
        <nav class="cd-dropdown dropdown-is-active-main">
            <h2>Категории</h2>
            <a href="#0" class="cd-close">Close</a>
            <ul class="cd-dropdown-content">
                @foreach($categories as $category)
                    @include('partials.category_item', ['category' => $category, 'show_category' => true , 'show_category_link' => true])
                @endforeach
            </ul>
        </nav>
    </div>
</div>