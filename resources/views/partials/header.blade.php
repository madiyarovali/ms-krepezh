@php
$phone = \App\Contact::where('key', 'phone')->first()->value;
$phones = explode(',', $phone);
$address = \App\Contact::where('key', 'address')->first()->value;

$cities = \App\City::all();
@endphp
<header>
    <section class="first_nav">
        <div class="container">
            <div class="row first_nav-row">
                <div class="first_nav-wrap">
                    <nav id="headerNav">
                        <button class="close-menu_btn" onclick="closeMenuMobie()">
                            <img src="{{ asset('assets/img/close-menu.svg') }}">
                        </button>
                        <li><a href="/">Главная </a></li>
                        <li><a href="/about_us">О компании </a></li>
                        
                        <li><a href="/delivery">Доставка </a></li>
                        <li><a href="/how_order">Как заказать?</a></li>
                      
                        @auth
                        <li><a href="/home">Личный кабинет</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Выйти
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        @else
                        <li><a href="/login">Войти</a></li>
                        <li><a href="/register">Регистрация</a></li>
                        @endauth
                        
                        <li><a href="#">{{ setting('site.last_update') }}</a></li>
                    </nav>
                    <nav>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $cities->where('id', session('city_id', 1))->first()->name }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @foreach ($cities as $city)
                                    <a class="dropdown-item" href="/change/city?city_id={{ $city->id }}">{{ $city->name }}</a>
                                @endforeach
                            </div>
                        </li>
                    </nav>
                    <div class="location-header">
                        <li>{{ setting('site.schedule') }}</li>
                        <li>
                            <img src="{{ asset('assets/img/location.svg') }}">
                            <p>{{ strip_tags($address) }}</p>
                        </li>
                    </div>
                    <div class="first_nav_contact">
                        @foreach($phones as $phone)
                        <h4><a href="tel:{{ $phone }}">{{ $phone }}</a></h4>
                        @endforeach
                        <button class="last-nav_contact-btn" onclick='$(".fast-order").addClass("fast-order-visible");'> Оставить заявку</button>
                    </div>
                </div>

                   <button class="mobile-menu_btn" onclick="openMenuMobie()">
            <img src="{{ asset('assets/img/menu.svg') }}">
        </button>

            </div>
        </div>
    </section>
    <section class="last_nav">
        <div class="container">
            <div class="row">
                <div class="last_nav-wrap">
                    <div class="logo text-center">
                        <a href="/"><img src="{{ asset('storage/'.setting('site.logo')) }}"
                            style="width: {{ setting('site.logo_size')  }}px !important">
                        {!!
                            setting('site.logo_text')
                        !!}</a>
                    </div>
                    <button class="btn-fast_order btn-fast_order-hover-effect" onclick='$(".fast-order").addClass("fast-order-visible");'>
                        <img src="{{ asset('assets/img/fast-btn.svg') }}">
                        <span>Быстрый заказ</span>
                    </button>
                    <form action="/search">
                        <div class="search-input">
                            <div class="search__input">
                                <input type="text" name="search" placeholder="Поиск по товарам" value="{{ $query ?? '' }}">
                                <p>Например:
                                    <a href="/search?search=шурупы"> шурупы</a>,
                                    <a href="/search?search=гвозди">гвозди</a>
                                </p>
                            </div>
                            <button type="submit">Найти </button>
                        </div>
                    </form>
                    <button class="searh-input_moblie" onclick="searchInputToggle()">
                        <img src="{{ asset('assets/img/search-input_mobile.svg') }}">
                        <img src="{{ asset('assets/img/close-search.svg') }}">
                    </button>
                    <div class="last-nav_contact" style="width: 12%;">
                        @foreach($phones as $phone)
                        <h4>{{ $phone }}</h4>
                        @endforeach
                        <a href="#"  onclick='$(".fast-order").addClass("fast-order-visible");'> Оставить заявку</a>
                    </div>
                    <div class="box-wrap">
                        <button class="btn-box">
                            <img src="{{ asset('assets/img/box-btn.svg') }}">
                            <span>Корзина</span>
                            <p class="count">{{ ShoppingCart::all()->count() }}</p>
                        </button>
                        <div class="box-products">
                            <a class="close_box-product"><img src="{{ asset('assets/img/close.svg') }}"></a>
                            <h5>Добавлено товаров на сумму: <b> {{ ShoppingCart::total() }} </b></h5>
                            <div class="box-products-list">
                                @include('partials.cart_item', ['from_controller' => true])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  
</header>