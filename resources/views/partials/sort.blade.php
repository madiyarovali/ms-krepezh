<div class="popular-products-nav">
    <a href="">Главная</a>
    <div class="popular-products-sort">
        <button class="popular-products-sort-btn">
            <img src="{{ asset('assets/img/sort.svg') }}">
            <span>Сортировать</span>
        </button>
        <div class="popular-products-sort-list">
            <li><a href="?sort=price&order=ASC">Сортировать по цене (возр)</a></li>
            <li><a href="?sort=price&order=DESC">Сортировать по цене (убыв)</a></li>
            <li><a href="?sort=name&order=ASC">Сортировать по наименованию (а-я)</a></li>
            <li><a href="?sort=name&order=DESC">Сортировать по наименованию (я-а)</a></li>
        </div>
    </div>
    <div class="popular-products-sort_view">
        <button class="trans-4"><img src="{{ asset('assets/img/view1.svg') }}"></button>
        <button class="trans-3"><img src="{{ asset('assets/img/view2.svg') }}"></button>
    </div>
</div>