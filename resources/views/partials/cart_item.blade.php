@if (ShoppingCart::all()->count() > 0)
    @if (isset($from_controller) and $from_controller)
        @foreach(ShoppingCart::all() as $item)
            @php
            $item_model = Product::find($item->id);
            if ($item->service) {
                $item_model = Service::find($item->id);
            }
            @endphp
            <li>
                <div class="img">
                    <img src="{{ asset("storage/$item_model->images") }}">
                </div>
                <div class="box-products-list-info">
                    <a href="">{{ $item->name }} </a>
                    <a href="">Артикул: {{ $item_model->article }}</a>
                    <div class="popular-product__btn-number">
                        <span>Кол-во:</span>
                        <input type="button" value="-" id="moins" class="minus-product control-btns" data-row-id="{{ $item->__raw_id }}">
                        <input type="text" size="25" value="{{ $item->qty }}" class="count-product">
                        <input type="button" value="+" id="plus" class="plus-product control-btns" data-row-id="{{ $item->__raw_id }}">
                        <button class="box-products-list_close" title="Удалить" data-row="{{ $item->__raw_id }}">
                            <img src="{{ asset('assets/img/trash.svg') }}">
                        </button>
                    </div>
                    <div class="box-products-list-price">
                        <span>{{ $item->price }}</span>
                    </div>
                </div>
            </li>
        @endforeach
    @else
        @php
        $item_model = Product::find($item->id);
        if ($item->service) {
            $item_model = Service::find($item->id);
            if ($item_model == null) {
                $item_model = Product::find($item->id);
            }
        }
        @endphp
        <li>
            <div class="img">
                <img src="{{ asset("storage/$item_model->images") }}">
            </div>
            <div class="box-products-list-info">
                <a href="">{{ $item->name }} </a>
                <a href="">Артикул: {{ $item_model->article }}</a>
                <div class="popular-product__btn-number">
                    <span>Кол-во:</span>
                    <input type="button" value="-" id="moins" class="minus-product">
                    <input type="text" size="25" value="{{ $item->qty }}" class="count-product">
                    <input type="button" value="+" id="plus" class="plus-product">
                    <button class="box-products-list_close" title="Удалить"><img
                            src="{{ asset('assets/img/trash.svg') }}"></button>
                </div>
                <div class="box-products-list-price">
                    <span>{{ $item->price }}</span>
                </div>
            </div>
        </li>
    @endif
    <div style="width: 100%;height: 100%;text-align: center;margin: 20px 0px;">
        <a href="/checkout" style="color: black;text-decoration: none;padding: 1rem 2.3rem;border: 1px solid darkgray;border-radius: 7px;">Оформить заказ</a>
    </div>
@else
    <li class="box-product_item">
        <h6 class="text-center" style="margin: 4rem auto;">Корзина пустая</h6>
    </li>
@endif