@php
    $slider = \App\Slider::where('position', 'slider')->first();
@endphp
<div class="row categories-slide">
    <div class="col-md-3 col-xs-12">
        <div class="categories">
            <h1>Категории</h1>
            <div class="cd-dropdown-wrapper">
                <a class="cd-dropdown-trigger" href="#0">Категории</a>
                <nav class="cd-dropdown dropdown-is-active-main">
                    <h2>Категории</h2>
                    <a href="#0" class="cd-close">Close</a>
                    <ul class="cd-dropdown-content">
                        @foreach($categories as $category)
                            @include('partials.category_item', ['category' => $category, 'show_category' => true , 'show_category_link' => true])
                        @endforeach
                    </ul>
                </nav>
            
                  
                
            </div>
        </div>
    </div>
    <div class="col-md-9 col-xs-12">
        <div class="banner">
            <div id="carousel" class="owl-carousel-banner owl-carousel">
                @foreach($slider->images as $image)
                <div class="banner-item">
                    <div class="banner-item_img">
                        <img src="{{ asset("storage/$image->path") }}">
                    </div>
                    <div class="banner-item_info">
                        {!! $image->title !!}
                        <a href="{!! $image->link !!}">
                            {{ $image->link_text ?? 'Посмотреть каталог' }}
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>