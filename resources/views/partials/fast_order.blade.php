<div class="fast-order-form_welcome">
        <h1>Быстрый заказ</h1>
        <form action="/send" method="post">
            @csrf     
            <label>Ваше имя</label>
            <input type="text" name="name" placeholder="Введите Ваше имя">
            <label>Ваш телефон</label>
            <input type="tel" name="phone" placeholder="+7 " class="phone-mask" maxlength="18">
            <label>Cообщение</label>
            <textarea   name="comment"></textarea>
            <button>Отправить</button>
        </form>
    </div>