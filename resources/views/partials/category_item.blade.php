@if ($category->children->count() > 0)
<li class="{{ $show_category_link || $category->children->count() > 0 ? 'has-children' : '' }} " style="">
	<a class="category-title" href="/category/{{ $category->id }}" style="overflow: hidden; text-overflow: ellipsis;
    white-space: nowrap;">{{ $category->name }}</a>
    <ul class="{{ $show_category ? 'cd-secondary-dropdown ' : '' }}is-hidden" style="overflow-x: hidden;">
    	<li class="go-back"><a href="#0">{{ $category->name }}</a></li>
		@foreach($category->children()->orderBy('order', 'ASC')->get() as $child)
			@include('partials.category_item', ['category' => $child, 'show_category' => false ,  'show_category_link' => false])
		@endforeach
	</ul>
</li>
@else

<li class="s">
	<a href="/category/{{ $category->id }}">{{ $category->name }}</a>
</li>
@endif

