<div class="form-order">
    <h1 class="form-title">Оформить заказ</h1>
    <form action="/make/order" method="POST">
        @csrf
        <label>Ваше имя <span class="star">*</span></label>
        <input type="text" name="customer_name" placeholder="Введите Ваше имя">
        <label>Ваш телефон <span class="star">*</span></label>
        <input type="tel" name="customer_phone" placeholder="+7 " class="phone-mask">
        <label>Электронная почта</label>
        <input type="email" name="customer_email" placeholder="Введите Ваш email">
        <label>Адрес доставки <span class="star">*</span></label>
        <input type="text" name="delivery_address" placeholder="Город, улица, дом">
        <label>Комментарий к заказу</label>
        <textarea rows="4" cols="50" placeholder="" name="order_note"></textarea>
        <ul class="form-order_total">
            <li>
                <p>Итог корзины</p>
                <span>{{ ShoppingCart::total() }}</span>
            </li>
            @if (request()->user() != null)
            <li class="d-none bonus">
                <p>Бонусы</p>
                <span>-{{ request()->user()->bonus }}</span>
            </li>
            @endif
            <li class="total-li">
                <p>Сумма заказа</p>
                @if (intval(setting('site.delivery_range')) > ShoppingCart::total())
                <span class="all">~{{ ShoppingCart::total() + setting('site.delivery_cost') }}</span>
                @else
                <span class="all">~{{ ShoppingCart::total() }}</span>
                @endif
                <span class="not-all d-none">~{{ ShoppingCart::total() }}</span>
            </li>
            {{-- <li class="total-li">
                <p>Сумма заказа</p>
                <span>{{ ShoppingCart::total() + 1000 }}</span>
            </li> --}}
            {{-- @if (request()->user() != null)
            <li class="total-li d-none">
                <p>Сумма заказа</p>
                <span>{{ ShoppingCart::total() + 1000 - request()->user()->bonus }}</span>
            </li>
            @endif --}}
        </ul>
        <div class="form-order_checbox">
            @if (request()->user() != null && setting('site.bonus_switcher'))
            <div class="custom-checkbox">
                <input type="checkbox" id="use-bonus" name="bonus">
                <label for="use-bonus"> Использовать бонусы ({{ request()->user()->bonus }})</label>
            </div>
            @endif
            <div class="custom-checkbox">
                <input type="checkbox" id="form-delivery" name="delivery" checked>
                <label for="form-delivery"> С доставкой (цену придется уточнить с менеджером)</label>
            </div>
            <div class="custom-checkbox">
                <input type="checkbox" id="form-order_checbox" name="accept" checked>
                <label for="form-order_checbox"> Соглашаюсь на обработку персональных данных в соответствии с Условиями, а также принимаете Оферту</label>
            </div>
        </div>
        <button class="form-order-btn">Потвердить заказ</button>
    </form>
</div>