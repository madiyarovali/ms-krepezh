@php
    $brands = \App\Slider::where('position', 'brands')->first();
@endphp
<div class="row">
    <div class="col-md-12">
        <div class="brand-logos">
            @foreach($brands->images as $image)
                <img src="{{ asset("storage/$image->path") }}">
            @endforeach
        </div>
    </div>
</div>