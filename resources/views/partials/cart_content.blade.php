<div class="order-box">
    <h1 class="form-title">Корзина</h1>
    <div class="delete-all">
        <a href="#">
            <img src="{{ asset('assets/img/trash.svg') }}">Удалить все</a>
    </div>
    <ul class="order-box_goods">
        @include('partials.checkout_items')
    </ul>
    <div class="order-box-goods_price">
        <div>
            <p><span class="count">{{ ShoppingCart::all()->count() }}</span> - товарa(ов)</p>
        </div>
        <div>
            <p>Итого:</p>
            <h3 class="total">{{ ShoppingCart::total() }}</h3>
        </div>
    </div>
</div>