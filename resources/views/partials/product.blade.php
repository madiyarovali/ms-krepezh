<div class="row">
    <div class="col-md-3">
        @include('partials.categories')
    </div>
    <div class="col-md-4 col-xs-12">
        <div class="product-img">
            <img src="{{ asset("storage/$product->images") }}">
        </div>
    </div>
    <div class="col-md-5 col-xs-12">
        <div class="product-characteristic">
            <h1>{{ $product->name }}</h1>
            <h6>код: {{ $product->id }}</h6>
            @if ($product->amount > 0)
            <div class="product-presence">
                <img src="{{ asset('assets/img/check.svg') }}">
                <span>есть в наличии</span>
            </div>
            @endif
            @if ($product->description)
            <div class="product-more_info">
                <h3>Дополнительная информация</h3>
                <div class="product-more-info_list">
                    {!! $product->description !!}
                </div>
            </div>
            @endif
            <div class="product-characteristic-price-wrap">
            <div class="product-characteristic-price d-inline">
                <span>Цена:</span>
                <p>
                    @if ($product->price)
                    {{ $product->price }}
                    @elseif (isset($options) && ($options->first() != null) && (isset($options->first()->price)))
                    {{ $options->first()->price }}
                    @endif
                </p>
            </div>
            <p class="product-characteristic-price_size">{{ $product->measure ?? '' }}</p>
            </div>
            <div class="product-characteristic-price">
                <span>{{ setting('site.price_text') }}  </span>
                <select name="" id="" class="form-control vars w-75">
                    @if ($options->count() > 0)
                    @foreach($options as $option)
                    <option value="{{ $option->id }}" price={{ $option->price }}>{{ $option->option }} - {{ $option->price }} тг</option>
                    @endforeach
                    @else
                    <option value="-1" price={{ $product->price }}>{{ $product->price }} тг</option>
                    @endif
                </select>  
            </div>
            <div class="popular-product_btns product-characteristi-number">
                <span>Кол-во:</span>
                <div class="popular-product__btn-number">
                    <input type="button" value="-" id="moins" class="minus-product">
                    <input type="text" size="25" value="1" class="count-product">
                    <input type="button" value="+" id="plus" class="plus-product">
                </div>              
                <button
                    class="btn-box_product add_to_cart_btn" 
                    data-product-id="{{ $product->id }}"
                    data-product-price="0"
                    data-product-option="0"
                    data-product-name="{{ $product->name }}">
                    <img src="{{ asset('assets/img/box-btn.svg') }}">
                    <span>В Корзину</span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs nav-tabs-products" role="tablist">
            <li class="nav-item">
                <a class="nav-link nav-link-product active" data-toggle="tab" href="#tabs-1" role="tab">Описание  </a>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-product" data-toggle="tab" href="#tabs-2" role="tab">Оплата </a>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-product" data-toggle="tab" href="#tabs-3" role="tab"> Доставка</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane tab-product active" id="tabs-1" role="tabpanel">
                {!! $product->full_description !!}
            </div>
            <div class="tab-pane tab-product" id="tabs-2" role="tabpanel">
                {!! setting('site.payment_text') !!}
            </div>
            <div class="tab-pane tab-product" id="tabs-3" role="tabpanel">
                {!! setting('site.delivery_text') !!}
            </div>
        </div>
    </div>
</div>