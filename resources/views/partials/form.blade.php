<div class="container">
    <div class="row">
        <div class="col-md-6 col-xs-12">
            @include('partials.checkout_form')
        </div>
        <div class="col-md-6 col-xs-12">
            @include('partials.cart_content')
        </div>
    </div>
</div>
<section class="container">
    @include('partials.recommendations')
</section>