@if ($products->count() > 0)
@if (!isset($hide_row)) <div class="row"> @endif
    @foreach($products as $product)
    @if (!isset($hide_row)) <div class="col-md-3 col-md-4 col-sm-4 col-xs-12 col-product"> @endif
        <div class="popular-product">
            <div class="favorite-product" title="избранное"></div>
            <div class="popular-product__img">
                <a href="/product/{{ $product->id }}"><img src="{{ asset("storage/$product->images") }}"></a>
            </div>
            <h1 class="popular-product__title">
                <a href="/product/{{ $product->id }}">{{ $product->name }}</a>
            </h1>
            @if (setting('site.price_switcher'))
            <div class="popular-product__price">
                @if (is_numeric($product->price))
                <span> {{ $product->price }}</span> <b>({{ $product->measure }})</b>
                @elseif ($product->options->count() > 0)
                    {{-- @if (abs($product->options->last()->price - $product->options->first()->price) > 0)
                        <span> {{ $product->options->first()->price }}</span> <b>({{ $product->measure }})</b> -
                        <span> {{ $product->options->last()->price }}</span> <b>({{ $product->measure }})</b>
                    @else --}}
                        <span> {{ $product->options->first()->price }}</span> <b>({{ $product->measure }})</b>
                    {{-- @endif --}}
                @endif
            </div>
            @endif
            @if (setting('site.btn_switcher'))
            <div class="popular-product_btns">
                <div class="popular-product__btn-number">
                    <input type="button" value="-" id="moins" class="minus-product">
                    <input type="text" size="25" value="1" class="count-product">
                    <input type="button" value="+" id="plus" class="plus-product">
                </div>
                <button class="btn-box_product add_to_cart_btn" data-product-id="{{ $product->id }}" data-product-price="{{ $product->price }}" data-product-name="{{ $product->name }}">
                    <span>В Корзину</span>
                </button>
            </div>
            @endif
        </div>
    @if (!isset($hide_row)) </div> @endif
    @endforeach
@if (!isset($hide_row)) </div> @endif
<div class="d-block">
    @if(method_exists($products, 'links'))
    {{ $products->links() }}
    @endif
</div>
@else
<div class="row justify-content-center">
    <div class="col-md-5 col-xs-12">
        <h3 class="text-muted my-5">
            Здесь пока нет товаров
        </h3>
    </div>
</div> 
@endif
