@php
$instagram = \App\Contact::where('key', 'instagram')->first()->value;
$facebook = \App\Contact::where('key', 'facebook')->first()->value;
$odnoklassniki = \App\Contact::where('key', 'odnoklassniki')->first()->value;
@endphp
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>{{ setting('site.title') }}</title>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	  <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/logo2.png">
	  <link rel="icon" type="image/png" sizes="16x16" href="/assets/img/logo2.png">
       <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/all.v55.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}?{{ uniqid() }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/notiflix-1.9.1.min.css?v=25') }}">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165091509-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', "{{ setting('site.google_analytics_tracking_id') }}");
      </script>
      {!! setting('site.codes') !!}

   </head>
   <body>
         <div class="float-sm">
          <div class="fl-fl float-fb fa-icon-main-instagram">
          <i class="fab fa-instagram fa-icon-main"></i>
            <a href="{{ $instagram }}" target="_blank"> подписывайтесь</a>
          </div>
          <div class="fl-fl float-tw fa-icon-main-facebook">
            <i class="fab fa-facebook-f fa-icon-main"></i>
            <a href="{{ $facebook }}" target="_blank">подписывайтесь</a>
          </div>
        </div>  
    @include('partials.header')

    <div class="wrapper">
        @yield('content')
    </div>
    @include('partials.footer')
    @include('partials.scripts') 
    @if (isset($message) and $message != '')
    <script>
        let title = '{{ $message }}' == 'Спасибо вам за отзыв' ? '' : 'Спасибо за заказ';
        Swal.fire({
            title: title,
            html: '{{ session("message") }}',
            icon: 'success',
            confirmButtonText: 'Ok'
        })
    </script>
    @endif

 
    </body>
</html>