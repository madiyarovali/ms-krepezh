@extends('layouts.app')

@section('content')
<section class="container">
    <div class="row row-mobile_order">
        <div class="col-md-3 col-xs-12">
            @include('partials.categories')
        </div>
        <div class="col-md-9 col-xs-12">
            <section class="register">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="register-title">Регистрация на сайте</div>
                        <div class="register-text">Все данные введенные вами будут проверяться техническим отделом. Недействительные аккаунты будут удалятся.
                            <br>
                            <br> Нажимая "Зарегистрироваться" вы принимаете условия пользовательского соглашения </div>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <form class="register-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="register-form_item">
                                <label>ФИО*</label>
                                <input type="text" name="name">
                                @error('name')
                                <span class="form_error">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="register-form_item">
                                <label>Email*</label>
                                <input type="text" name="email" value="">
                                @error('email')
                                <span class="form_error">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="register-form_item">
                                <label>Номер телефона*</label>
                                <input type="text" name="phone" value="" class="phone-mask">
                                @error('phone')
                                <span class="form_error">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="register-form_item">
                                <label>Пароль</label>
                                <input type="password" name="password" value="">
                                @error('password')
                                <span class="form_error">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="register-form_item">
                                <label>Повторите пароль</label>
                                <input type="password" name="password_confirmation" value="">
                            </div>

                            <div class="custom-checkbox register-checkbox">
                                <input type="checkbox" id="form-register_checbox" required>
                                <label for="form-register_checbox"> Я даю свое согласие на обработку моих персональных данных.</label>
                            </div>

                            <a href="/login" class="link-activate">У вас уже есть аккаунт?</a>

                            <input type="submit" value="Зарегистрироваться" class="register-form-btn">
                        </form>
                    </div>
                </div>
            </section>

        </div>
    </div>
</section>
@endsection
