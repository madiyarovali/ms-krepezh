@extends('layouts.app')

@section('content')
<section class="container">
    <div class="row row-mobile_order">
        <div class="col-md-3 col-xs-12">
            @include('partials.categories')
        </div>
        <div class="col-md-9 col-xs-12">
            <section class="register">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="register-title">Вход на сайт</div>
                        <div class="register-text">Войдите на свой личный кабинет</div>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <form class="register-form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="register-form_item">
                                <label>Email*</label>
                                <input type="text" name="email" value="">
                                @error('email')
                                <span class="form_error">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="register-form_item">
                                <label>Пароль</label>
                                <input type="password" name="password" value="">
                                @error('password')
                                <span class="form_error">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <a href="/recovery" class="link-activate">Забыли пароль?</a>
                            <a href="/register" class="link-activate">У вас нету аккаунта?</a>

                            <input type="submit" value="Вход" class="register-form-btn">
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

@endsection
