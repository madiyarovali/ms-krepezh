@extends('layouts.app')

@section('content')
    <section class="container">
        <div class="row categories-slide">
            <div class="col-md-3 col-xs-12">
                @include('partials.categories')
                @include('partials.filters')
            </div>
            <div class="col-md-9 col-xs-12">
                <div class="popular-products">
                    @include('partials.sort')
                    @if($category->children && $category->children->count() > 0)
                        <div class="my-4">
                            <h4>
                                Подкатегории
                            </h4>
                            @foreach($category->children as $child_t)
                            <a href="/category/{{ $child_t->id }}">
                                {{ $child_t->name }} ({{ $child_t->products->count() }})
                            </a>
                            @endforeach
                        </div>
                    @endif
                    @include('partials.products', ['products' => $products])
                </div>
            </div>
			 <div class="col-md-12 col-xs-12 categories-mobile">
                @include('partials.categories')
            </div>
        </div>
    </section>
    <section class="container">
        @include('partials.loader')
    </section>
    <input type="hidden" class="page-name" value="category-{{ $category->id }}">
@endsection