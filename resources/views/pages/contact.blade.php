@extends('layouts.app')

@section('content')
<section class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumbs">
                <li>
                    <a href="/"><img src="{{ asset('assets/img/home.svg') }}">Главная</a>
                </li>
                <li>
                    <span>Контакты</span>
                </li>
            </ul>
        </div>
    </div>
</section>
{!! setting('site.contacts') !!}
@endsection