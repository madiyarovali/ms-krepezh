@extends('layouts.app')

@section('content')
<section class="container my-2">
    <div class="row">
        <div class="col-md-3">
            <div class="user_data">
                <h1 class="user_data-name">
                    {{ $user->name }}
                </h1>
                <h6 class="user_data-title">
                     Электронная почта:
                  </h6>
                <p class="user_data-info">
                    {{ $user->email }}
                </p>
                {{-- <h6 class="user_data-title">
                     Номер телефона:
                  </h6>
                <p class="user_data-info">
                    {{ $user->phone }}
                </p> --}}
                {{-- <a href="" class="user_data-btn">Редактировать</a> --}}
            </div>
            @if (setting('site.bonus_switcher'))
            <div class="user_data-bonus">
                <h1>Бонусы</h1>
                {{-- <div class="user_data-bonus-info">
                    <p>Номер карты:</p>
                    <span> отсутствует</span>
                </div> --}}
                <div class="user_data-bonus-info">
                    <p>Бонусы:</p>
                    <span> {{ $user->bonus }}</span>
                </div>
                <p><b>1 бонус = 1 тенге.</b> Оплатить бонусами можно до половины стоимости заказа.</p>
            </div>
            @endif
        </div>
        <div class="col-md-9">
            <div class="personal_area-products">
                <ul class="nav nav-tabs nav-tabs-personal_area" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link nav-link-personal_area active" data-toggle="tab" href="#my_goods-tab" role="tab">Мои заказы </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link nav-link-personal_area" data-toggle="tab" href="#my_favorites-tab" role="tab">
                            Избранное
                            <img src="{{ asset('assets/img/favorite-active.svg') }}">
                        </a>
                    </li> --}}
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane tab-personal_area active" id="my_goods-tab" role="tabpanel">
                        <div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">
                            @foreach($user->orders as $index => $order)
                            <div class="panel">
                                <div class="panel-heading" role="tab" id="heading{{ $index }}">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $index }}" aria-expanded="true" aria-controls="collapse{{ $index }}" class="personal-area_order-block-title">
                                        <h2 class="title">Заказ №<b>  {{ $order->id }}</b></h2>
                                        <h2 class="data">
                                       Дата: <b>{{ $order->created_at->format('d.m.Y') }} г.</b>
                                    </h2>
                                        <h2 class="status">
                                       Статус:<b class="expectations"> {{ $order->status }}</b> 
                                    </h2>
                                        <a href=""></a>
                                    </a>
                                </div>
                                <div id="collapse{{ $index }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{{ $index }}">
                                    <div class="panel-body">
                                        <ul class="order-box_goods order-personal-area_links">
                                            @foreach($order->booking_items as $item)
                                            @if ($item->product_id != null && $item->product)
                                            <li>
                                                <div class="order-product-info">
                                                    <div class="img">
                                                        <img src="{{ asset("storage/". $item->product->images) }}">
                                                    </div>
                                                    <div class="content">
                                                        <a href="/product/{{ $item->product->id }}">{{ $item->product->name }}</a>
                                                        <p>Артикул: {{ $item->product->article }}</p>
                                                    </div>
                                                </div>
                                                <div class="personal_area_price">
                                                    <p><b>{{ $item->quantity }}</b> x <b>{{ $item->price }}</b> </p>
                                                </div>
                                            </li>
                                            @else
                                            <li>
                                                <div class="order-product-info">
                                                    <div class="content">
                                                        <p>Доставка</p>
                                                    </div>
                                                </div>
                                                <div class="personal_area_price">
                                                    <p><b>1</b> x <b>1000</b> </p>
                                                </div>
                                            </li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    {{-- <div class="tab-pane tab-personal_area" id="my_favorites-tab" role="tabpanel">
                        <h1>Отличный выбор!</h1>
                        <p>Если Вам нравится товар, но сейчас покупать его не хочется, можно сохранить его в список избранного на будущее.</p>
                        <div class="personal_area-favourite-table">
                            <div class="order-box">
                                <ul class="order-box_goods">
                                    <li>
                                        <div class="order-product-info">
                                            <div class="img">
                                                <img src="assets/img/popular_product.svg">
                                            </div>
                                            <div class="content">
                                                <a href="">Муфта соед. профиль 60х27</a>
                                                <p>Артикул: 2098988</p>
                                            </div>
                                        </div>
                                        <div class="popular-product__btn-number">
                                            <span>Кол-во:</span>
                                            <input type="button" value="-" id="moins" class="minus-product">
                                            <input type="text" size="25" value="1" class="count-product">
                                            <input type="button" value="+" id="plus" class="plus-product">
                                        </div>
                                        <button class="btn-box_favourite">
                                            <img src="assets/img/box-btn.svg">
                                            <span>В Корзину</span>
                                        </button>
                                    </li>
                                    <li>
                                        <div class="order-product-info">
                                            <div class="img">
                                                <img src="assets/img/popular_product4.svg">
                                            </div>
                                            <div class="content">
                                                <a href="">Муфта соед. профиль 60х27</a>
                                                <p>Артикул: 2098988</p>
                                            </div>
                                        </div>
                                        <div class="popular-product__btn-number">
                                            <span>Кол-во:</span>
                                            <input type="button" value="-" id="moins" class="minus-product">
                                            <input type="text" size="25" value="1" class="count-product">
                                            <input type="button" value="+" id="plus" class="plus-product">
                                        </div>
                                        <button class="btn-box_favourite">
                                            <img src="assets/img/box-btn.svg">
                                            <span>В Корзину</span>
                                        </button>
                                    </li>
                                    <li>
                                        <div class="order-product-info">
                                            <div class="img">
                                                <img src="assets/img/popular_product2.svg">
                                            </div>
                                            <div class="content">
                                                <a href="">Муфта соед. профиль 60х27</a>
                                                <p>Артикул: 2098988</p>
                                            </div>
                                        </div>
                                        <div class="popular-product__btn-number">
                                            <span>Кол-во:</span>
                                            <input type="button" value="-" id="moins" class="minus-product">
                                            <input type="text" size="25" value="1" class="count-product">
                                            <input type="button" value="+" id="plus" class="plus-product">
                                        </div>
                                        <button class="btn-box_favourite">
                                            <img src="assets/img/box-btn.svg">
                                            <span>В Корзину</span>
                                        </button>
                                    </li>
                                    <li>
                                        <div class="order-product-info">
                                            <div class="img">
                                                <img src="assets/img/popular_product.svg">
                                            </div>
                                            <div class="content">
                                                <a href="">Муфта соед. профиль 60х27</a>
                                                <p>Артикул: 2098988</p>
                                            </div>
                                        </div>
                                        <div class="popular-product__btn-number">
                                            <span>Кол-во:</span>
                                            <input type="button" value="-" id="moins" class="minus-product">
                                            <input type="text" size="25" value="1" class="count-product">
                                            <input type="button" value="+" id="plus" class="plus-product">
                                        </div>
                                        <button class="btn-box_favourite">
                                            <img src="assets/img/box-btn.svg">
                                            <span>В Корзину</span>
                                        </button>
                                    </li>
                                    <li>
                                        <div class="order-product-info">
                                            <div class="img">
                                                <img src="assets/img/popular_product4.svg">
                                            </div>
                                            <div class="content">
                                                <a href="">Муфта соед. профиль 60х27</a>
                                                <p>Артикул: 2098988</p>
                                            </div>
                                        </div>
                                        <div class="popular-product__btn-number">
                                            <span>Кол-во:</span>
                                            <input type="button" value="-" id="moins" class="minus-product">
                                            <input type="text" size="25" value="1" class="count-product">
                                            <input type="button" value="+" id="plus" class="plus-product">
                                        </div>
                                        <button class="btn-box_favourite">
                                            <img src="assets/img/box-btn.svg">
                                            <span>В Корзину</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
{{-- <section class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="recommend-title">
                <h1>Рекомендуем</h1>
            </div>
        </div>
        <div class="col-md-12">
            <div id="recommend-slider" class="owl-carousel-banner owl-theme owl-carousel owl-carousel-recommend">
                <div class="popular-product">
                    <div class="favorite-product" title="избранное"></div>
                    <div class="popular-product__img">
                        <img src="assets/img/popular_product.svg">
                    </div>
                    <h1 class="popular-product__title">
                        Саморезы для тонких пластин Standers 
                     </h1>
                    <div class="popular-product__price">
                        <span> 115</span> <b>(уп.)</b>
                    </div>
                    <div class="popular-product_btns">
                        <div class="popular-product__btn-number">
                            <input type="button" value="-" id="moins" class="minus-product">
                            <input type="text" size="25" value="1" class="count-product">
                            <input type="button" value="+" id="plus" class="plus-product">
                        </div>
                        <button class="btn-box_product">
                            <span>В Корзину</span>
                        </button>
                    </div>
                </div>
                <div class="popular-product">
                    <div class="favorite-product" title="избранное"></div>
                    <div class="popular-product__img">
                        <img src="assets/img/popular_product3.svg">
                    </div>
                    <h1 class="popular-product__title">
                        Саморезы для тонких пластин Standers 
                     </h1>
                    <div class="popular-product__price">
                        <span> 115</span> <b>(уп.)</b>
                    </div>
                    <div class="popular-product_btns">
                        <div class="popular-product__btn-number">
                            <input type="button" value="-" id="moins" class="minus-product">
                            <input type="text" size="25" value="1" class="count-product">
                            <input type="button" value="+" id="plus" class="plus-product">
                        </div>
                        <button class="btn-box_product">
                            <span>В Корзину</span>
                        </button>
                    </div>
                </div>
                <div class="popular-product">
                    <div class="favorite-product" title="избранное"></div>
                    <div class="popular-product__img">
                        <img src="assets/img/popular_product2.svg">
                    </div>
                    <h1 class="popular-product__title">
                        Саморезы для тонких пластин Standers 
                     </h1>
                    <div class="popular-product__price">
                        <span> 115</span> <b>(уп.)</b>
                    </div>
                    <div class="popular-product_btns">
                        <div class="popular-product__btn-number">
                            <input type="button" value="-" id="moins" class="minus-product">
                            <input type="text" size="25" value="1" class="count-product">
                            <input type="button" value="+" id="plus" class="plus-product">
                        </div>
                        <button class="btn-box_product">
                            <span>В Корзину</span>
                        </button>
                    </div>
                </div>
                <div class="popular-product">
                    <div class="favorite-product" title="избранное"></div>
                    <div class="popular-product__img">
                        <img src="assets/img/popular_product4.svg">
                    </div>
                    <h1 class="popular-product__title">
                        Саморезы для тонких пластин Standers 
                     </h1>
                    <div class="popular-product__price">
                        <span> 115</span> <b>(уп.)</b>
                    </div>
                    <div class="popular-product_btns">
                        <div class="popular-product__btn-number">
                            <input type="button" value="-" id="moins" class="minus-product">
                            <input type="text" size="25" value="1" class="count-product">
                            <input type="button" value="+" id="plus" class="plus-product">
                        </div>
                        <button class="btn-box_product">
                            <span>В Корзину</span>
                        </button>
                    </div>
                </div>
                <div class="popular-product">
                    <div class="favorite-product" title="избранное"></div>
                    <div class="popular-product__img">
                        <img src="assets/img/popular_product.svg">
                    </div>
                    <h1 class="popular-product__title">
                        Саморезы для тонких пластин Standers 
                     </h1>
                    <div class="popular-product__price">
                        <span> 115</span> <b>(уп.)</b>
                    </div>
                    <div class="popular-product_btns">
                        <div class="popular-product__btn-number">
                            <input type="button" value="-" id="moins" class="minus-product">
                            <input type="text" size="25" value="1" class="count-product">
                            <input type="button" value="+" id="plus" class="plus-product">
                        </div>
                        <button class="btn-box_product">
                            <span>В Корзину</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
@endsection