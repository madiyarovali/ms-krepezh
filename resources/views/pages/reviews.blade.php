@extends('layouts.app')

@php
$reviews = \App\Review::where('is_active', 1)->get();
@endphp
@section('content')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<section class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumbs">
                <li>
                    <a href="/">
                        <img src="assets/img/home.svg">Главная</a>
                </li>
                <li>
                    <span> Отзывы</span>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-3 col-xs-12">
         @include('partials.categories')
        </div>
        <div class="col-md-9 col-xs-12">
            @foreach ($reviews as $review)
                <div class="card p-4">
                    <p class="d-flex mb-0" style="align-items: center">
                        <span class="font-italic">{{ $review->author }}:</span>
                    </p>
                    <p class="font-weight-bold mb-0 ml-4">
                        {{ $review->text }}
                        @if ($review->rate)
                        <br>
                        Оценка: {{ $review->rate }}
                        @endif
                    </p>
                    @if ($review->comment)
                    <div style="border-left: 2px solid gray; padding-left: 14px">
                        <p class="d-flex mb-0" style="align-items: center">
                            <span class="font-italic">Ответ менеджера:</span>
                        </p>
                        <p class="font-weight-bold mb-0 ml-4">
                            {{ $review->comment }}
                        </p>
                    </div>
                    @endif
                </div>
            @endforeach
            <form action="/leave/review" class="mt-5" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="author">Ваше имя</label>
                    <input type="text" class="form-control" id="author" placeholder="Ваше имя" name="author">
                </div>
                <div class="form-group">
                    <label for="">Ваше сообщение</label>
                    <input type="text" name="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Ваша оценка</label>
                    <select class="form-control" name="rate">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <div class="g-recaptcha" data-sitekey="6LdfBu0UAAAAAHSbUAs8P0cgfZ9T3GxYFH-btJ4z"></div>
                <button type="submit" class="btn-fast_order btn-fast_order-hover-effect">Отправить</button>
            </form>
        </div>

        
    </div>
</section>
@endsection