<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>MS крепеж</title>
      <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
      <link rel="stylesheet" type="text/css" href="assets/css/all.css">
   </head>
   <body>
      <header>
         <section class="first_nav">
            <div class="container">
               <div class="row">
                  <div class="first_nav-wrap">
                     <nav id="headerNav">
                        <button class="close-menu_btn" onclick="closeMenuMobie()">
                        <img src="assets/img/close-menu.svg">
                        </button>
                        <li><a href="/">Главная </a></li>
                        <li><a href="/about_us"> О компании </a></li>
                        
                        <li><a href="/delivery">Доставка  </a></li>
                        <li><a href="/how_order"> Как заказать?</a></li>
                        
                     </nav>
                     <div class="location-header">
                        <li>Работаем с 9:00 - 20:00</li>
                        <li>
                           <img src="assets/img/location.svg"> 
                           <p>г. Алматы, пр. Рыскулова 94</p>
                        </li>
                     </div>
                     <div class="first_nav_contact">
                        <h4>+7(727)375 19 19</h4>
                        <a href="/"> Оставить заявку</a>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="last_nav">
            <div class="container">
               <div class="row">
                  <div class="last_nav-wrap">
                     <div class="logo">
                        <img src="assets/img/logo.svg">
                     </div>
                     <button class="btn-fast_order">
                     <img src="assets/img/fast-btn.svg">
                     <span>Быстрый заказ</span>
                     </button>
                     <div class="search-input">
                        <div class="search__input">
                           <input type="text" name="search" placeholder="Поиск по товарам">
                           <p>Например: 
                              <a href="/"> шурупы</a>,
                              <a href="/">гвозди</a>
                           </p>
                        </div>
                        <button>Найти </button>
                     </div>
                     <button class="searh-input_moblie" onclick="searchInputToggle()">
                     <img src="assets/img/search-input_mobile.svg">
                     <img src="assets/img/close-search.svg">
                     </button>
                     <div class="last-nav_contact">
                        <h4>+7(727)375 19 19</h4>
                        <a href="/"> Оставить заявку</a>
                     </div>
                     <div class="box-wrap">
                        <button class="btn-box">
                           <img src="assets/img/box-btn.svg">
                           <span>Корзина</span>
                           <p>2</p>
                        </button>
                        <div class="box-products">
                           <a class="close_box-product"><img src="assets/img/close.svg"></a>
                           <h5>Добавлено товаров на сумму: <b> 3 436 </b></h5>
                           <div class="box-products-list">
                              <li>
                                 <div class="img">
                                    <img src="assets/img/popular_product2.svg">
                                 </div>
                                 <div class="box-products-list-info">
                                    <a href="">Саморезы для тонких пластин Standers </a>
                                    <a href="">Артикул: 2098988</a>
                                    <div class="popular-product__btn-number">
                                       <span>Кол-во:</span>  
                                       <input type="button" value="-" id="moins" class="minus-product" >
                                       <input type="text" size="25" value="1" class="count-product">
                                       <input type="button" value="+" id="plus" class="plus-product">
                                       <button class="box-products-list_close" title="Удалить"><img src="assets/img/trash.svg"></button>
                                    </div>
                                    <div class="box-products-list-price">
                                       <span>775</span>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="img">
                                    <img src="assets/img/popular_product3.svg">
                                 </div>
                                 <div class="box-products-list-info">
                                    <a href="">Саморезы для тонких пластин Standers </a>
                                    <a href="">Артикул: 2098988</a>
                                    <div class="popular-product__btn-number">
                                       <span>Кол-во:</span>  
                                       <input type="button" value="-" id="moins" class="minus-product" >
                                       <input type="text" size="25" value="1" class="count-product">
                                       <input type="button" value="+" id="plus" class="plus-product">
                                       <button class="box-products-list_close" title="Удалить"><img src="assets/img/trash.svg"></button>
                                    </div>
                                    <div class="box-products-list-price">
                                       <span>775</span>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="img">
                                    <img src="assets/img/popular_product4.svg">
                                 </div>
                                 <div class="box-products-list-info">
                                    <a href="">Саморезы для тонких пластин Standers </a>
                                    <a href="">Артикул: 2098988</a>
                                    <div class="popular-product__btn-number">
                                       <span>Кол-во:</span>  
                                       <input type="button" value="-" id="moins" class="minus-product" >
                                       <input type="text" size="25" value="1" class="count-product">
                                       <input type="button" value="+" id="plus" class="plus-product">
                                       <button class="box-products-list_close" title="Удалить"><img src="assets/img/trash.svg"></button>
                                    </div>
                                    <div class="box-products-list-price">
                                       <span>775</span>
                                    </div>
                                 </div>
                              </li>
                           </div>
                           <button>Перейти в корзину</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="mobile-nav">
            <button class="mobile-menu_btn" onclick="openMenuMobie()">
            <img src="assets/img/menu.svg">
            </button>
         </section>
      </header>
      <section class="container">
         <div class="row">
            <div class="col-md-12">
               <ul class="breadcrumbs">
                  <li>
                     <a href="/">
                     <img src="assets/img/home.svg">Главная</a>
                  </li>
                  <li>
                     <span> Магазины</span>
                  </li>
               </ul>
            </div>
         </div>
      </section>
      <section class="container">
         <div class="row">
            <div class="col-md-3 col-xs-12">
               <div class="categories">
                  <h1>Категории</h1>
                  <div class="categories_links">
                     <a><p href="">сухие смеси и грунтовки</p></a>
                     <a><p href="">Листовые материалы</p></a>
                     <a><p href="">Блоки для строительства</p></a>
                     <a><p href="">изоляционные материалы</p></a>
                     <a><p href="">кровля</p></a>
                     <a><p href="">Столярные изделия</p></a>
                     <a><p href="">Скобяные изделия</p></a>
                     <a><p href="">Краски</p></a>
                  </div>
               </div>
            </div>
            <div class="col-md-9 col-xs-12">
               <div class="magazine">
                  <h1 class="delivery-title">
                     Наши магазины
                  </h1>
                  <div class="row magazine-contact_block">
                    <div class="col-md-6 col-xs-12">
                         <div class="item">
                        <div class="item-card">
                           <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa1ca0bc416489d376a9840d5424c2f43c12e2adf877e29460c869a270a16ab4d&amp;source=constructor" width="100%" height="100%" frameborder="0"></iframe>
                        </div>
                        <div class="item-info">
                           <h1 class="title">
                              Алматы
                           </h1>
                           <div class="contact">
                              <img src="assets/img/phone.svg">
                              <div class="contact-info">
                                 <h3>Телефон</h3>
                                 <a href="tel:+77273751919">+7 (727) 3751919</a>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/place.svg">
                              <div class="contact-info">
                                 <h3>Адрес</h3>
                                 <p>пр. Рыскулова 94</p>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/clock.svg">
                              <div class="contact-info">
                                 <h3>График работы</h3>
                                 <p>Ежедневно: 8:00 - 20:00</p>
                              </div>
                           </div>
                        </div>
                     </div>
                    </div>
                         <div class="col-md-6 col-xs-12">
                         <div class="item">
                        <div class="item-card">
                           <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa1ca0bc416489d376a9840d5424c2f43c12e2adf877e29460c869a270a16ab4d&amp;source=constructor" width="100%" height="100%" frameborder="0"></iframe>
                        </div>
                        <div class="item-info">
                           <h1 class="title">
                            Нур-султан
                           </h1>
                           <div class="contact">
                              <img src="assets/img/phone.svg">
                              <div class="contact-info">
                                 <h3>Телефон</h3>
                                 <a href="tel:+77273751919">+7 (727) 3751919</a>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/place.svg">
                              <div class="contact-info">
                                 <h3>Адрес</h3>
                                 <p>пр. Рыскулова 94</p>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/clock.svg">
                              <div class="contact-info">
                                 <h3>График работы</h3>
                                 <p>Ежедневно: 8:00 - 20:00</p>
                              </div>
                           </div>
                        </div>
                     </div>
                    </div>
                   
                           <div class="col-md-6 col-xs-12">
                         <div class="item">
                        <div class="item-card">
                           <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa1ca0bc416489d376a9840d5424c2f43c12e2adf877e29460c869a270a16ab4d&amp;source=constructor" width="100%" height="100%" frameborder="0"></iframe>
                        </div>
                        <div class="item-info">
                           <h1 class="title">
                             Актобе
                           </h1>
                           <div class="contact">
                              <img src="assets/img/phone.svg">
                              <div class="contact-info">
                                 <h3>Телефон</h3>
                                 <a href="tel:+77273751919">+7 (727) 3751919</a>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/place.svg">
                              <div class="contact-info">
                                 <h3>Адрес</h3>
                                 <p>пр. Рыскулова 94</p>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/clock.svg">
                              <div class="contact-info">
                                 <h3>График работы</h3>
                                 <p>Ежедневно: 8:00 - 20:00</p>
                              </div>
                           </div>
                        </div>
                     </div>
                    </div>

                           <div class="col-md-6 col-xs-12">
                         <div class="item">
                        <div class="item-card">
                           <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa1ca0bc416489d376a9840d5424c2f43c12e2adf877e29460c869a270a16ab4d&amp;source=constructor" width="100%" height="100%" frameborder="0"></iframe>
                        </div>
                        <div class="item-info">
                           <h1 class="title">
                             Атырау
                           </h1>
                           <div class="contact">
                              <img src="assets/img/phone.svg">
                              <div class="contact-info">
                                 <h3>Телефон</h3>
                                 <a href="tel:+77273751919">+7 (727) 3751919</a>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/place.svg">
                              <div class="contact-info">
                                 <h3>Адрес</h3>
                                 <p>пр. Рыскулова 94</p>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/clock.svg">
                              <div class="contact-info">
                                 <h3>График работы</h3>
                                 <p>Ежедневно: 8:00 - 20:00</p>
                              </div>
                           </div>
                        </div>
                     </div>
                    </div>

                           <div class="col-md-6 col-xs-12">
                         <div class="item">
                        <div class="item-card">
                           <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa1ca0bc416489d376a9840d5424c2f43c12e2adf877e29460c869a270a16ab4d&amp;source=constructor" width="100%" height="100%" frameborder="0"></iframe>
                        </div>
                        <div class="item-info">
                           <h1 class="title">
                          Караганда
                           </h1>
                           <div class="contact">
                              <img src="assets/img/phone.svg">
                              <div class="contact-info">
                                 <h3>Телефон</h3>
                                 <a href="tel:+77273751919">+7 (727) 3751919</a>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/place.svg">
                              <div class="contact-info">
                                 <h3>Адрес</h3>
                                 <p>пр. Рыскулова 94</p>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/clock.svg">
                              <div class="contact-info">
                                 <h3>График работы</h3>
                                 <p>Ежедневно: 8:00 - 20:00</p>
                              </div>
                           </div>
                        </div>
                     </div>
                    </div>


                           <div class="col-md-6 col-xs-12">
                         <div class="item">
                        <div class="item-card">
                           <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa1ca0bc416489d376a9840d5424c2f43c12e2adf877e29460c869a270a16ab4d&amp;source=constructor" width="100%" height="100%" frameborder="0"></iframe>
                        </div>
                        <div class="item-info">
                           <h1 class="title">
                         Петропавлск
                           </h1>
                           <div class="contact">
                              <img src="assets/img/phone.svg">
                              <div class="contact-info">
                                 <h3>Телефон</h3>
                                 <a href="tel:+77273751919">+7 (727) 3751919</a>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/place.svg">
                              <div class="contact-info">
                                 <h3>Адрес</h3>
                                 <p>пр. Рыскулова 94</p>
                              </div>
                           </div>
                           <div class="contact">
                              <img src="assets/img/clock.svg">
                              <div class="contact-info">
                                 <h3>График работы</h3>
                                 <p>Ежедневно: 8:00 - 20:00</p>
                              </div>
                           </div>
                        </div>
                     </div>
                    </div>

                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Кнопка связи -->
      <div class="whatsap-wrap_btn">
         <div class="whatsapp_btn">
            <a class="whatsapp-btn_pulse">
            <img src="assets/img/whatsapp.svg"> <span>Написать на WhatsApp</span></a>
         </div>
      </div>
      <div class="follow-me_instagram">
         <div class="follow-me_instagram-content">
            <a class="follow-me_instagram-close"><img src="assets/img/close.svg"></a>
            <p>Подпишись на наш Instagram <a href="">@ms.krepezh</a> страницу, лайкни наш пост получи бонус</p>
         </div>
      </div>
      <div class="fast-order">
         <div class="fast-order-form">
            <button class="close-fast-order"><img src="assets/img/close.svg"></button>
            <h1>Быстрый заказ</h1>
            <form>
               <label>Ваше имя</label>
               <input type="text" name="name" placeholder="Введите Ваше имя">
               <label>Ваш телефон</label>
               <input type="tel" name="phone" placeholder="+7 ">
               <label>Cообщение</label>
               <textarea  rows="4" cols="50" placeholder="Мне нужен ....">
               </textarea>
               <button>Отправить</button>
            </form>
         </div>
      </div>
      <footer>
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-xs-12">
                  <div class="footer-about">
                     <img src="assets/img/logo.svg">
                     <p>TOO “MS крепеж” работает с 2012 года по продаже крепежных и строительных товаров.  Идейные соображения высшего порядка, а также реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации новых предложений.</p>
                     <span>© Copyright MS крепеж</span>
                  </div>
               </div>
               <div class="col-md-3 col-xs-12">
                  <div class="footer-nav">
                     <div class="footer-links">
                        <a href="">Главная </a>
                        <a href="">О компании </a>
                        <a href="">Магазины </a>
                        <a href="">Доставка </a>
                        <a href="">Контакты </a>
                     </div>
                     <div class="footer-links">
                        <a href="">Договор оферты</a>
                        <a href="">Как заказать?   </a>
                        <a href="">Оформить заказ </a>
                        <a href="">Корзина</a>
                     </div>
                  </div>
               </div>
               <div class="col-md-3 col-xs-12">
                  <div class="footer-magazine">
                     <h1>Магазины</h1>
                     <div class="footer-magazine-wrap">
                        <div class="footer-magazine-col">
                           <h2>г. Алматы</h2>
                           <p>пр. Рыскулова 94 <br>
                              +7 (727) 3751919
                           </p>
                           <h2>г. Актобе</h2>
                           <p>ул.  Бекетай 1/1<br>
                              +7 (727) 3751919
                           </p>
                           <h2>г. Караганда</h2>
                           <p>пр. Рыскулова 94 <br>
                              +7 (727) 3751919
                           </p>
                        </div>
                        <div class="footer-magazine-col">
                           <h2>г. Нурсултан</h2>
                           <p>пр. Рыскулова 94 <br>
                              +7 (727) 3751919
                           </p>
                           <h2>г. Атырау</h2>
                           <p>ул.  Бекетай 1/1<br>
                              +7 (727) 3751919
                           </p>
                           <h2>г. Петропавлск</h2>
                           <p>пр. Рыскулова 94 <br>
                              +7 (727) 3751919
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-3 col-xs-12">
                  <div class="footer-contact">
                     <h1>+7 (727) 3751919</h1>
                     <a href=""> Оставить заявку</a>
                     <h2>г. Алматы <br>
                        пр. Рыскулова 94
                     </h2>
                     <h6>Мы в соц. сетях</h6>
                     <a href=""> <img src="assets/img/instagram.svg"></a>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
      <script type="text/javascript" src="assets/js/ion.rangeSlider.min.js"></script>
      <script type="text/javascript" src="assets/js/rangeSlider.js"></script>
      <script type="text/javascript" src="assets/js/mine.js"></script>
      <script src='assets/js/owl.carousel.min.js'></script>
      <script type="text/javascript" src="assets/js/banner-slider.js"></script>
   </body>
</html>