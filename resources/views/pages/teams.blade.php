@extends('layouts.app')

@php
$teams = \App\Team::all();
@endphp
@section('content')
<section class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumbs">
                <li>
                    <a href="/">
                        <img src="assets/img/home.svg">Главная</a>
                </li>
                <li>
                    <span> Строительные бригады</span>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-3 col-xs-12">
         @include('partials.categories')
        </div>
        <div class="col-md-9 col-xs-12 my-3">
            @foreach ($teams as $team)
                <div class="card p-4">
                    <p class="d-flex" style="align-items: center">
                        <span class="font-weight-bold">{{ $team->name }}</span>
                    </p>
                    <p class="font-italic mb-0">
                        {!! $team->description !!}
                    </p>
                    <p class="text-center w-25 mb-0">
                        <a href="tel:{{ $team->phone }}" class="btn-fast_order btn-fast_order-hover-effect justify-content-center">Позвонить</a>
                    </p>
                </div>
            @endforeach
        </div>

        
    </div>
</section>
@endsection