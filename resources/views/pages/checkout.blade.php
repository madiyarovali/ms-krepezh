@extends('layouts.app')

@section('content')
    <section class="form">
        @include('partials.form')
    </section>
@endsection