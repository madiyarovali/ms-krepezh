@extends('layouts.app')

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                       <h3>Корзина</h3>
                        <ul>
                            <li><a href="/">Главная</a></li>
                            <li>Корзина</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
    <!--breadcrumbs area end-->

     <!--shopping cart area start -->
    <div class="shopping_cart_area mt-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="table_desc">
                        <div class="cart_page table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="product_remove">Удалить</th>
                                        <th class="product_thumb">Картинка</th>
                                        <th class="product_name">Наименование</th>
                                        <th class="product-price">Цена</th>
                                        <th class="product_quantity">Количество</th>
                                        <th class="product_total">Итог</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(ShoppingCart::all() as $item)
                                    @php
                                    $item_model = Product::find($item->id);
                                    if ($item->service) {
                                        $item_model = Service::find($item->id);
                                    }
                                    @endphp
                                    <tr>
                                        <td class="product_remove"><a href="{{ route('cart.remove') }}" data-row="{{ $item->__raw_id }}"><i class="fa fa-trash-o"></i></a></td>
                                        <td class="product_thumb">
                                            <a href="/products/{{ $item->id }}"><img src="{{ asset('storage/'. $item_model->images) }}" alt=""></a>
                                        </td>
                                        <td class="product_name"><a href="/products/{{ $item->id }}">{{ $item->name }}</a></td>
                                        <td class="product-price">{{ $item_model->price }} ₸</td>
                                        <td class="product_quantity">
                                            <label>Количество </label>
                                            <button data-row-id="{{ $item->__raw_id }}" data-qty="{{ $item->qty - 1 }}" class="control-btns minus">-</button>
                                            <span>{{ $item->qty }}</span>
                                            <button data-row-id="{{ $item->__raw_id }}" data-qty="{{ $item->qty + 1 }}" class="control-btns add">+</button>
                                        </td>
                                        <td class="product_total">{{ $item->total }} ₸</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>   
                        </div> 
                    </div>
                 </div>
             </div>
             <!--coupon code area start-->
            <div class="coupon_area">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code right">
                            <h3>Итого</h3>
                            <div class="coupon_inner">
                               <div class="cart_subtotal">
                                   <p class="price">{{ ShoppingCart::total() }} </p>
                               </div>
                               <div class="checkout_btn">
                                   <a href="/checkout">Оформить заказ</a>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </div>
     <!--shopping cart area end -->
    
@endsection
    