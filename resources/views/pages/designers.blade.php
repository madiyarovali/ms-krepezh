@extends('layouts.app')

@php
$designers = \App\Designer::all();
@endphp
@section('content')
<section class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumbs">
                <li>
                    <a href="/">
                        <img src="assets/img/home.svg">Главная</a>
                </li>
                <li>
                    <span> Дизайнеры</span>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-3 col-xs-12">
         @include('partials.categories')
        </div>
        <div class="col-md-9 col-xs-12 my-3">
            @foreach ($designers as $designer)
                <div class="card p-4">
                    <p class="d-flex" style="align-items: center">
                        <a href="{{ $designer->url }}">
                            <span class="font-weight-bold">{{ $designer->name }}</span>
                        </a>
                    </p>
                    <p class="font-italic mb-0">
                        {!! $designer->description !!}
                    </p>
                </div>
            @endforeach
        </div>

        
    </div>
</section>
@endsection