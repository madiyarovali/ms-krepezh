@extends('layouts.app')

@section('content')
    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><img src="{{ asset('assets/img/home.svg') }}">Главная</a>
                    </li>
                    @foreach($product->categories as $category)
                    <li>
                        <a href="/category/{{ $category->id }}"> {{ $category->name }}</a>
                    </li>
                    @endforeach
                    <li>
                        <span>{{ $product->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="container">
        @include('partials.product')
    </section>
    
    <section class="container">
        @include('partials.recommendations')
    </section>
@endsection