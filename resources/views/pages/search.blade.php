@extends('layouts.app')

@section('content')
    <section class="container">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                @include('partials.categories')
                @include('partials.filters')
            </div>
            <div class="col-md-9 col-xs-12">
                <div class="popular-products">
                    @include('partials.sort')
                    @include('partials.products', ['products' => $products])
                </div>
            </div>
        </div>
    </section>
    <section class="container">
        @include('partials.loader')
    </section>
    <input type="hidden" class="page-name" value="search-{{ $query }}">
@endsection