@extends('layouts.app')

@section('content')
<section class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumbs">
                <li>
                    <a href="/">
                        <img src="{{ asset('assets/img/home.svg') }}">Главная</a>
                </li>
                <li>
                    <span> Как заказать?</span>
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="container">
    <div class="row">
        <div class="col-md-3 col-xs-12">
            @include('partials.categories')
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="how_order">
                {!! setting('site.how_order') !!}
            </div>
        </div>
    </div>
</section>
@endsection