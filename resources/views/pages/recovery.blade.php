@extends('layouts.app')

@section('content')
       <section class="container">
         <div class="row">
            <div class="col-md-12">
               <ul class="breadcrumbs">
                  <li>
                     <a href="/">
                     <img src="assets/img/home.svg">Главная</a>
                  </li>
                  <li>
                     <span> Регистрация</span>
                  </li>
                 
            </ul>
            </div>
         </div>
      </section>


   <section class="container">
         <div class="row row-mobile_order">
            <div class="col-md-3 col-xs-12">
               <div class="categories">
                  <h1>Категории</h1>
                  <div class="categories_links">
                     <li><a href="">сухие смеси и грунтовки</a></li>
                     <li><a href="">Листовые материалы</a></li>
                     <li><a href="">Блоки для строительства</a></li>
                     <li><a href="">изоляционные материалы</a></li>
                     <li><a href="">кровля</a></li>
                     <li><a href="">Столярные изделия</a></li>
                     <li><a href="">Скобяные изделия</a></li>
                     <li><a href="">Краски</a></li>
                  </div>
               </div>
            </div>
            <div class="col-md-9 col-xs-12">
      <section class="register">
   <div class="row">
   <div class="col-md-5 col-xs-12">
      <div class="register-title">Забыли пароль?</div>
      <div class="register-text">   
      Если на сайте существует учётная запись с адресом  вашей электронной почты вы получите письмо со ссылкой, позволяющей сменить пароль.                           
         </div>
   </div>
   <div class="col-md-7 col-xs-12">
      <form class="register-form">
       
         <div class="register-form_item">
            <label>Email*</label> 
           <input type="text" name="email" value="">
            <span class="form_error">
            </span>
         </div>
       
        
<input type="submit" value="
Восстановить пароль" class="register-form-btn">
      </form>
   </div>
</div>
</section>

         </div>
   </section>




@endsection