@extends('layouts.app')

@section('content')
<section class="container">
    @include('partials.categories_with_slider')
</section>

<section class="container">
    @php
        $catalog = \App\Slider::where('position', 'main')->first();
    @endphp
    <div class="row">
        <div class="col-md-3 col-xs-12">
            @include('partials.fast_order')
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="popular-products">
                <h1>Популярные товары</h1>
                @include('partials.products', ['products' => $catalog->products()->limit(6)->paginate(13)])
            </div>
        </div>
    </div>
</section>

<section class="container">
     @include('partials.services')
</section>

<section class="container">
    @include('partials.loader')
</section>

<section class="container">
    @include('partials.advantages')
</section>

{{-- <section class="container">
    @include('partials.about')
</section> --}}

{{-- @include('partials.instagram') --}}

<input type="hidden" class="page-name" value="welcome">



<section class="animation-car">
    <div class="car-driver-wrap">
    <img src="https://static.tildacdn.com/tild6166-3132-4930-b334-646332626366/22.png" class="car-driver">
    </div>
        <div class="man-driver-wrap">
            <img src=" {{ asset('assets/img/car2.png') }}" class="man-driver">
        </div>

    </section>
@endsection