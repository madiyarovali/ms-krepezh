@extends('layouts.app')

@section('content')
<section class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumbs">
                <li>
                    <a href="/">
                        <img src="assets/img/home.svg">Главная</a>
                </li>
                <li>
                    <span> Доставка</span>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-3 col-xs-12">
         @include('partials.categories')
        </div>
        <div class="col-md-9 col-xs-12">
            {!! setting('site.delivery') !!}
        </div>
    </div>
</section>
@endsection