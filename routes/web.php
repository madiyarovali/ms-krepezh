<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/product/{product_id}', 'CommonController@getProduct');
    Route::get('/products/{page}', 'CommonController@getProducts');

    Route::get('/', 'CommonController@index');
    Route::get('/test', 'CommonController@test');
    Route::get('/checkout', 'CommonController@checkout');
    Route::get('/search', 'CommonController@search');
    Route::get('/cart', 'CommonController@cart');
    Route::get('/wishlist', 'CommonController@wishlist');
    Route::get('/product/{product_id}', 'CommonController@product')->name('product.index');
    Route::get('/category/{category_id}', 'CommonController@category')->name('category.index');
    Route::get('/change/city', 'CommonController@changeCity');

    Route::get('/cart/buy', 'CartController@buy')->name('cart.buy');
    Route::post('/send', 'CartController@send');
    Route::post('/cart/add', 'CartController@add')->name('cart.add');
    Route::post('/cart/update', 'CartController@update')->name('cart.update');
    Route::post('/cart/remove', 'CartController@remove')->name('cart.remove');
    Route::post('/cart/add/from/quick', 'CartController@fromQuick')->name('cart.quick');
    Route::post('/make/order', 'CartController@order');
    Route::post('/leave/review', 'CartController@review');

    Route::get('/admin/qpalreqw/import', 'ImportController@index');
    Route::get('/admin/qpalreqw/export', 'ExportController@create');
    Route::post('/admin/qpalreqw/import', 'ImportController@create');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Auth::routes();

Route::get('/qpalreqw', 'CommonController@update');
Route::get('/qpalreqw1', 'ImportController@modify');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{page}', function ($page) {
    return view('pages.'.$page);
});
