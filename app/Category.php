<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'parent_id'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product', 'products_categories', 'category_id', 'product_id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service', 'services_categories', 'category_id', 'service_id');
    }

    public function all_products()
    {
        $products = [];
        $categories = [$this];
        while(count($categories) > 0){
            $nextCategories = [];
            foreach ($categories as $category) {
                $products = array_merge($products, $category->products->all());
                $nextCategories = array_merge($nextCategories, $category->children->all());
            }
            $categories = $nextCategories;
        }
        return new Collection($products);
    }
}
