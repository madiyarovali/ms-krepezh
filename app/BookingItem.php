<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingItem extends Model
{
    protected $fillable = [
        'booking_id',
        'name',
        'price',
        'quantity',
        'product_id',
        'details'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
