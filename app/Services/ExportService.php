<?php

namespace App\Services;

use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportService
{
    public static function rows()
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }
}