<?php

namespace App\Services;

use App\{ Product, ProductOption, Category };

class ExcelService
{
    protected $gg;
    protected $g;
    protected $category;
    protected $product;
    protected $size;
    protected $price;
    protected $image;

    public function process($rows)
    {
        ini_set('max_execution_time', 300);
        // Product::where('id', '>', 1)->delete();
        // ProductOption::where('id', '>', 1)->delete();
        // Category::where('id', '>', 1)->delete();
        $products = Product::all();
        foreach ($products as $key => $product) {
            foreach ($rows as $index => $row) {
                if ($product->name == $this->getProduct($row[4]) && $rows[$index + 1][6] != '' && !is_numeric($rows[$index + 1][6])) {
                    $product->measure = $rows[$index + 1][6];
                    $product->save();
                }
            }    
        }
        dd(Product::where('measure', '<>', null)->count());
        // foreach ($rows as $index => $row) {
        //     if ($row[4] == '') continue;
        //     $product = Product::where('name', $row[4])->first();
        //     dd(Product::first());
            // if ($index <= 1) {
            //     continue;
            // }
            // if ($row[1] != null) {
            //     $this->setGG($row);
            // } elseif ($row[2] != null) {
            //     $this->setG($row);
            // } elseif ($row[3] != null) {
            //     $this->setCategory($row);
            // } elseif ($row[4] != null) {
            //     $this->setProduct($row);
            // }
            // $this->setDetails($row);
            // if ($this->getGG()) {
            //     $gg = Category::where('name', $this->getGG())->first();
            //     if ($gg == null) {
            //         $gg = Category::create(['name' => $this->getGG()]);
            //     }
            // }
            // if ($this->getG() && $gg) {
            //     $g = Category::where('name', $this->getG())->where('parent_id', $gg->id)->first();
            //     if ($g == null) {
            //         $g = Category::create(['name' => $this->getG(), 'parent_id' => $gg->id]);
            //     }
            // }
            // if ($this->getCategory() && $g) {
            //     $category = Category::where('name', $this->getCategory())->where('parent_id', $g->id)->first();
            //     if ($category == null) {
            //         $category = Category::create(['name' => $this->getCategory(), 'parent_id' => $g->id]);
            //     }
            // }
            // if ($this->getProduct() && $category) {
            //     $str = $this->getProduct();
            //     if (!mb_detect_encoding($str, 'UTF-8', true)) {
            //         $this->product = 'FAСADE';
            //     }
            //     $product = Product::where('name', $this->getProduct())->first();
            //     if ($product == null) {
            //         $product = Product::create(['name' => $this->getProduct(), 'category_id' => $category->id, 'images' => $this->getImage()]);
            //         \DB::table('products_categories')->insert([
            //             'product_id' => $product->id,
            //             'category_id' => $category->id,
            //         ]);
            //     }
            // }
            // if ($this->getDetails() && isset($product) && $product) {
            //     $detail = ProductOption::create(['product_id' => $product->id, 'price' => $this->getDetails()[0], 'option' => $this->getDetails()[1]]);
            // }
        // }
        // ProductOption::where('price', 0)->delete();
    }
    public function setGG($row)
    {
        $this->gg = $row[1];
    }

    public function setG($row)
    {
        $this->g = $row[2];
    }

    public function getGG()
    {
        return $this->gg;
    }

    public function getG()
    {
        return $this->g;
    }

    public function setDetails($row)
    {
        $this->size = $row[5];
        $this->price = $row[9];
    }

    public function getDetails()
    {
        $price = $this->price;
        $size = $this->size;
        if ($price == '') {
            $price = 0;
        }
        return [$price, $size];
    }

    public function setProduct($row)
    {
        $this->product = $row[4];
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getProduct($text = null)
    {
        $product = $text;
        if ($text == null) 
            $product = $this->product;
        preg_match('/\((.*)\)/', $product, $matches);
        if (sizeof($matches) > 1) {
            $this->image = $matches[1]. '.jpg';
        } else {
            $this->image = '';
        }
        $product = preg_replace('/\((.*)\)/', '', $product);
        return trim($product);
    }

    public function setCategory($row)
    {
        $this->category = $row[3];
    }

    public function getCategory()
    {
        return $this->category;
    }
}