<?php

namespace App\Services;

use App\{ Product, ProductOption, City };
use SimpleXLSX, SimpleXLS;

class ImportService
{
    public static function rows($path)
    {
        set_time_limit(3600);
        if ($xlsx = SimpleXLSX::parse($path)) {
            $result = [];
            foreach($xlsx->rows() as $index => $r) {
                for ($i = 1; $i < 6; $i++) {
                    if (isset($r[$i])) {
                        $cities[] = $i;
                    }
                }
                $cities = array_unique($cities);
                if ($index < 1) continue;

                foreach ($cities as $index1 => $city_name) {
                    $product = Product::where('name', $r[0])->first();
                    $product_option = ProductOption::where(['product_id' => $product->id, 'option' => $r[1]])->first();
                    $city = City::where('id', $city_name)->first();
                    if ($city && $product_option) {
                        if ($r[$city_name + 1] != "") {
                            $price = $r[$city_name + 1];
                        } else if (is_double($r[$city_name])) {
                            $price = floatval($r[$city_name]) + 0.01;
                        } else if ($product_option) {
                            $price = $product_option->price_old + 0.01;
                        } else {
                            $price = $product->price;
                        }
                        $result[] = [
                            'option_id' => $product_option->id,
                            'city_id' => $city->id,
                            'price' => $price
                        ];
                    }
                }
            }
            return $result;
        } else {
            echo SimpleXLSX::parseError();
            return;
        }
    }
}