<?php

namespace App\Exports;

use App\{ Product, City };
use Maatwebsite\Excel\Concerns\FromCollection;

class ProductsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $rows = [['Название', 'Вариант']];
        $cities = City::all();
        foreach ($cities as $city) {
            $rows[0][] = $city->name;
        }
        foreach (Product::all() as $index => $product) {
            $index += 1;
            if (!$product->options->count()) {
                $rows[$index]['product_name'] = $product->name;
                $rows[$index]['option_name'] = '';
                foreach ($cities as $city_index => $city) {
                    $rows[$index]["city_$city_index"] = $product->price;
                }
            }
            $temp = [];
            foreach ($product->options as $option) {
                $temp['product_name'] = $product->name;
                $temp['option_name'] = $option->option;
                foreach ($option->cities as $city_index => $city) {
                    $temp["city_$city_index"] = $city->pivot->price;
                }
                $rows[] = $temp;
            }
        }
        return collect($rows);
    }
}
