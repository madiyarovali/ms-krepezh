<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'images',
        'measure',
        'category_id'
    ];

    public function options()
    {
        return $this->hasMany('App\ProductOption');
    }

    public function sales()
    {
        return $this->hasMany('App\SpecialProduct');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'products_categories', 'product_id', 'category_id');
    }

    public function sliders()
    {
        return $this->belongsToMany('App\Slider', 'products_sliders');
    }

    public function recommendations()
    {
        return $this->belongsToMany('App\Product', 'products_recommendations', 'product_id', 'category_id');
    }

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('is_active', function (Builder $builder) {
    //         if (!(strpos(url()->current(), 'admin') !== false)) {
    //             $builder->where('is_active', 1);
    //         }
    //     });
    // }
}
