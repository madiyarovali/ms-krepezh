<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Filter extends Model
{
    public function options()
    {
        return $this->hasMany('App\FilterOption');
    }
}
