<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'customer_name',
        'customer_phone',
        'receiver_name',
        'receiver_phone',
        'delivery_address',
        'delivery_date',
        'delivery_time_range',
        'note_text',
        'delivery_region',
        'anonymity',
        'order_note',
        'user_id',
        'status',
        'customer_email',
        'delivery'
    ];

    public function booking_items()
    {
        return $this->hasMany('App\BookingItem');
    }
}
