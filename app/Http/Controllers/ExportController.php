<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{ ProductOption, Product };
use App\Services\ExportService;

class ExportController extends Controller
{
    public function create(Request $request)
    {
        return ExportService::rows();
    }
}