<?php

namespace App\Http\Controllers;

use App\{ Category, Product, Slider, Review, Filter, ProductOption };
use App\Services\{ InstagramService, ExcelService };
use Illuminate\Http\Request;
use ShoppingCart;
use SimpleXLS;

class CommonController extends Controller
{
    public function index()
    {
        $message = '';
        if (session('message')) {
            $message = session('message');
        }
        $service = new InstagramService;
        $raw_data = $service->getData();
        if ($raw_data[0] == '<') {
            $raw_data = $service->getContent();
        }
        $posts = $service->processed($raw_data, 12);
        $most_viewed = Product::orderBy('views', 'desc')->limit(10)->paginate(20);
        $reviews = Review::all();
        $categories = Category::where('parent_id', null)->orderBy('order', 'ASC')->get();
        return view('pages.welcome', compact('posts', 'categories', 'reviews', 'most_viewed', 'message'));
    }

    public function product($product_id)
    {
        $product = Product::findOrFail($product_id);
        $product->views++;
        $product->save();
        $services = [];
        $services_id = [];
        foreach ($product->categories as $category) {
            foreach ($category->services as $service) {
                if (!in_array($service->id, $services_id)) {
                    $services_id[] = $service->id;
                    $services[] = $service;
                }
            }
        }
        $most_viewed = Product::orderBy('views', 'desc')->limit(10)->get();
        $options = ProductOption::where('product_id', $product->id)->get();
        $id = $product->categories->pluck('id');
        $recommendations = $product->recommendations;
        $categories = Category::where('parent_id', null)->get();
        return view('pages.product', compact('product', 'services', 'most_viewed', 'recommendations', 'options', 'categories'));
    }

    public function category(Request $request, $id)
    {
        $category_page = true;
        $categories = Category::where('parent_id', null)->orderBy('order', 'ASC')->get();
        if ($id == 0) {
            $products = Product::where('id', '>', 0);
            $category = new \stdClass;
            $category->id = 0;
            $category->name = 'Каталог';
        } else {
            $category = Category::findOrFail($id);
            $products_temp = $category->all_products();
            $ids = [];
            foreach ($products_temp as $product_temp) {
                $ids = array_merge($ids, $product_temp->categories->pluck('id')->toArray());
            }
            $products = Product::whereHas('categories', function ($query) use ($id, $products_temp, $ids) {
                $query->whereIn('category_id', $ids);
            });
        }

        $per_page = $request->per_page ?? 12;
        $key = $request->sort ?? 'price';
        $value = $request->order ?? 'ASC';
        $products = $products->orderBy($key, $value);
        $products = $products->paginate($per_page);

        return view('pages.category', compact('category', 'categories', 'category_page', 'products', 'per_page', 'key', 'value'));
    }

    public function search(Request $request)
    {
        $query = $request->get('search');
        $products = Product::where('name', 'like', "%$query%");

        $per_page = $request->per_page ?? 10;
        $key = $request->key ?? 'views';
        $value = ($key == 'price.up') ? 'ASC' : 'DESC';
        $key = ($key == 'price.up' or $key == 'price.down') ? 'price' : $key;
        $products = $products->orderBy($key, $value);
        $categories = Category::all();

        $products = $products->get();
        return view('pages.search', compact('products', 'categories', 'per_page', 'key', 'value', 'query'));
    }

    public function checkout()
    {
        return view('pages.checkout');
    }

    public function getProduct($product_id)
    {
        $product = Product::findOrFail($product_id);
        $product->views++;
        $product->save();
        $services = [];
        $services_id = [];
        foreach ($product->categories as $category) {
            foreach ($category->services as $service) {
                if (!in_array($service->id, $services_id)) {
                    $services_id[] = $service->id;
                    $services[] = $service;
                }
            }
        }
        return view('partials.modal_content', compact('product', 'services'));
    }

    public function getProducts(Request $request, $page)
    {
        if ($page == 'welcome') {
            $slider = Slider::where('position', 'main')->first();
            $products = Product::whereHas('sliders', function ($query) use ($slider) {
                $query->where('slider_id', $slider->id);
            });
        } else if (strpos($page, 'search') !== false) {
            preg_match('/search-(.+)/', $page, $matches);
            $products = Product::where('name', 'like', "%$matches[1]%");
        } else if (strpos($page, 'category') !== false) {
            preg_match('/category-(.+)/', $page, $matches);
            $category_page = true;
            $id = $matches[1];
            if ($id == 0) {
                $products = Product::where('id', '>', 0);
                $category = new \stdClass;
                $category->id = 0;
                $category->name = 'Каталог';
            } else {
                $category = Category::findOrFail($id);

                $products = Product::whereHas('categories', function ($query) use ($id) {
                    $query->where('category_id', $id);
                });
            }
        }
        $filters = $request->filters;
        if ($filters) {
            $products = $products->where(function ($q) use ($filters, $products) {
                foreach ($filters as $index => $filter) {
                    $q->orWhere($filter['key'], $filter['val']);
                }
            });
        }
        $products = $products->whereBetween('price', [$request->from, $request->to]);
        $products = $products->whereBetween('length', [$request->dfrom, $request->dto]);
        $key = $request->sortBy ?? 'price';
        $value = $request->order ?? 'ASC';

        $products = $products->orderBy($key, $value);
        $products = $products->get();
        return view('partials.products', compact('products'));
    }

    public function makeOrder(Request $request)
    {
        $items = $request->all();
        return view('search-results', compact('items'));
    }

    public function test()
    {
        $path = 'storage/files/test.xls';
        if ( $xlsx = SimpleXLS::parse($path) ) {
            echo '<table border="1" cellpadding="3" style="border-collapse: collapse">';
            $service = new ExcelService;
            $service->process($xlsx->rows());
            foreach( $xlsx->rows() as $index => $r ) {
                echo '<tr><td>'.implode('</td><td>', $r ).'</td></tr>';
            }
            echo '</table>';
        } else {
            echo SimpleXLS::parseError();
        }
        // ===== // ===== // ===== // ===== // ===== // =====
        // $categories = Category::all();
        // foreach ($categories as $key => $category) {
        //     preg_match('/\((.*)\)/', $category->name, $matches);
        //     $file = '';
        //     if (sizeof($matches) > 1) {
        //         $file = $matches[1]. '.jpg';
        //         $category->name = preg_replace('/\((.*)\)/', '', $category->name);
        //         $category->save();
        //     }
        //     if ($file != '') {
        //         foreach ($category->products as $key => $product) {
        //             if ($product->image == 'products/placeholder.jpg') {
        //                 $product->image = 'products/'. $file;
        //             }
        //         }
        //     }
        // }
    }

    public function update(Request $request)
    {
        if ($request->pq != '159875321a') {
            return back();
        }
        $categories = Category::where('parent_id', null)->get();
        return view('pages.exporter', compact('categories'));
    }

    public function changeCity(Request $request)
    {
        session(['city_id' => $request->city_id]);
        return back();
    }
}
