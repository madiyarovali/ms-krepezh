<?php

namespace App\Http\Controllers;

use App\{ Category, Product, Slider, Review };
use Cart;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function add(Request $request)
    {
        $product = Product::find($request->product_id);
        Cart::instance('wishlist')->add([
            'id' => $request->product_id,
            'name' => $request->name,
            'qty' => $request->quantity,
            'price' => $request->price,
            'weight' => 0
        ]);
        return Cart::instance('wishlist')->content();
    }

    public function remove(Request $request)
    {
        Cart::instance('wishlist')->remove($request->row_id);
        return Cart::instance('wishlist')->content();
    }
}
