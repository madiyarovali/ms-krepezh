<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{ ProductOption, Product };
use App\Services\ImportService;

class ImportController extends Controller
{
    public function index()
    {
        return view('pages.import');
    }

    public function create(Request $request)
    {
        $path = $request->file('file')->store('public/file');
        $path = str_replace('public/', 'storage/', $path);
        $rows = ImportService::rows($path);
        foreach ($rows as $row) {
            $query = \DB::table('prices')->where('option_id', $row['option_id'])->where('city_id', $row['city_id']);
            if ($query->count() > 0) {
                $query->update([
                    'price' => $row['price']
                ]);
                if ($row['city_id'] == 1) {
                    ProductOption::where('id', $row['option_id'])->update([
                        'price_old' => $row['price']
                    ]);
                }
            } else {
                \DB::table('prices')->insert($row);
            }
        }

        return back();
    }
}