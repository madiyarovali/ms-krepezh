<?php

namespace App\Http\Controllers;

use App\{ Category, Product, Slider, Review, Service, Option, Booking, BookingItem, ProductOption };
use Cart, Mail, Carbon\Carbon;
use Illuminate\Http\Request;
use ShoppingCart;

class CartController extends Controller
{
    public function send(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required'
        ]);
        $has_link = stristr($request->comment, 'http://') ?: stristr($request->comment, 'https://');
        if ($has_link) {
            return redirect('/')->with('message', "Наш менеджер свяжется с Вами в течении 5 минут");
        }
        $has_link = stristr($request->comment, '.ru') ?: stristr($request->comment, '.com');
        if ($has_link) {
            return redirect('/')->with('message', "Наш менеджер свяжется с Вами в течении 5 минут");
        }

        $text = "Имя заказчика: $request->name\nНомер заказчика: $request->phone\nКомментарии: $request->comment";

        $email_receivers = \App\Contact::where('key', 'email_receivers')->first()->value;
        $emails = explode(',', $email_receivers);
        foreach ($emails as $index => $email) {
            $emails[$index] = trim($email);
        }
        $main_email = $emails[0];
        unset($emails[0]);
        Mail::raw($text, function ($message) use ($emails, $main_email) {
            $message->to($main_email);
            $message->cc($emails);
            $message->subject('Новая заявка');
        });

        return redirect('/')->with('message', "Наш менеджер свяжется с Вами в течении 5 минут");
    }

    public function review(Request $request)
    {
        $request->validate([
            'author' => 'required',
            'text' => 'required',
            'g-recaptcha-response' => 'required'
        ]);
        $data = $request->only([
            'author',
            'text',
            'rate',
        ]);
        \Log::info(json_encode($data));
        $data['avatar'] = ' ';
        $review = Review::create($data);

        return redirect('/')->with('message', "Спасибо вам за отзыв");
    }

    public function add(Request $request)
    {
        $product = Product::find($request->get('product_id'));
        if ($product->options->count() == 0) {
            $attributes = [
                'product_id' => $product->id,
            ];
            $price = ($product->sales->count() > 0) ? $product->sales->last()->sale / 100 * $product->price : $product->price;
        } else {
            $option_id = $request->option_id ?? $product->options->first()->id;
            $option = ProductOption::find($option_id);
            
            $attributes = [
                'product_id' => $product->id,
                'option_id' => $option_id
            ];
            $price = ($product->sales->count() > 0) ? $product->sales->last()->sale / 100 * $option->price : $option->price;
        }
        ShoppingCart::associate('App\Product');
        
        ShoppingCart::add($product->id, $product->name, intval($request->get('quantity')), $price, $attributes);
        $from_controller = true;
        return [
            'html' => view('partials.cart_item', compact('from_controller'))->render(),
            'count' => ShoppingCart::all()->count(),
            'total' => ShoppingCart::total()
        ];
    }

    public function remove(Request $request)
    {
        ShoppingCart::remove($request->row_id);
        $from_controller = true;
        return [
            'html' => view('partials.cart_item', compact('from_controller'))->render(),
            'count' => ShoppingCart::all()->count(),
            'total' => ShoppingCart::total()
        ];
    }

    public function update(Request $request)
    {
        ShoppingCart::update(
            $request->get('row_id'),
            $request->get('quantity')
        );
        $from_controller = true;

        return [
            'html' => view('partials.cart_item', compact('from_controller'))->render(),
            'count' => ShoppingCart::all()->count(),
            'total' => ShoppingCart::total()
        ];
    }

    public function buy(Request $request)
    {
        $product = Product::find($request->product_id);
        ShoppingCart::associate('App\Product');
        $price = ($product->sales->count() > 0) ? $product->sales->last()->price : $product->price;
        ShoppingCart::add($product->id, $product->name, 1, $price);
        return redirect('checkout');
    }

    public function fromQuick(Request $request)
    {
        $product = Product::find($request->get('product_id'));
        ShoppingCart::associate('App\Product');
        $price = ($product->sales->count() > 0) ? $product->sales->last()->price : $product->price;
        $attributes = [
            'product_id' => $product->id
        ];
        ShoppingCart::add($product->id, $product->name, intval($request->get('quantity')), $price, $attributes);
        foreach ($request->get('services') as $service) {
            if ($service == 'Нет, спасибо') {
                continue;
            }
            preg_match('/(\d+)-(\d+)/', $service, $matches);
            $service_id = $matches[1];
            $option_id = $matches[2];
            $service = Service::find($service_id);
            $option = Option::find($option_id);
            ShoppingCart::associate('App\Service');
            ShoppingCart::add($service_id, $option->name, 1, $option->price, ['service' => true]);
        }
        $from_controller = true;
        return [
            'html' => view('partials.cart_item', compact('from_controller'))->render(),
            'count' => ShoppingCart::all()->count(),
            'total' => ShoppingCart::total()
        ];
    }

    public function order(Request $request)
    {
        $request->validate([
            'accept' => 'required',
            'customer_name' => 'required',
            'customer_phone' => 'required'
        ]);
        if ($request->customer_name == 'WilliamDuh') {
            return redirect('/')->with('message', "Ваш номер заказа №1. Наш менеджер свяжется с Вами в течении 5 минут");
        }
        $delivery_cost = $request->delivery_cost ?? setting('site.delivery_range') > ShoppingCart::total() ? setting('site.delivery_cost') : 0;
        $payload = $request->except('_token', 'work_time', 'user_agreement');
        $total = ShoppingCart::total();
        if (isset($request->bonus) && setting('site.bonus_switcher')) {
            $total = ShoppingCart::total() - request()->user()->bonus;
            if ($payload['order_note'] == null) {
                $payload['order_note'] = '';
            }
            $payload['order_note'] .= "\nИспользовал бонусы: ". $request->bonus;
            $request->user()->bonus = 0;
            $request->user()->save();
        }

        if ($request->user() != null && setting('site.bonus_switcher')) {
            $payload['user_id'] = $request->user()->id;
            $request->user()->bonus += $total * setting('site.bonus_percent');
            $request->user()->save();
        }
        $payload['delivery'] = isset($payload['delivery']) && $payload['delivery'] == 'on';
        $payload['status'] = 'В ожидании';
        $booking = Booking::create($payload);
        ShoppingCart::associate('App\Service');
        ShoppingCart::add(0, 'доставка', 1, $delivery_cost);
        $list = "Список товаров: \n";
        foreach (ShoppingCart::all() as $item) {
            $option = ProductOption::find($item->option_id);
            $items = BookingItem::create([
                'booking_id' => $booking->id,
                'name' => $item->qty. 'x ' .$item->name. ' = ' . $item->total,
                'price' => $item->total,
                'quantity' => $item->qty,
                'product_id' => $item->product_id,
                'details' => $option ? $option->option : ''
            ]);
            $list .= "\t$items->name - $items->details\n";
        }
        
        $customer_name = $payload['customer_name'];
        $customer_phone = $payload['customer_phone'];
        $customer_email = $payload['customer_email'];
        $delivery_address = $payload['delivery_address'];
        $order_note = $payload['order_note'];
        
        $text = "Имя заказчика: $customer_name\nНомер заказчика: $customer_phone\nПочта заказчика: $customer_email\nАдрес доставки: $delivery_address\n$list\nКомментарии к заказу $order_note\nИтог: $total";
        
        $email_receivers = \App\Contact::where('key', 'email_order')->first()->value;
        $emails = explode(',', $email_receivers);
        foreach ($emails as $index => $email) {
            $emails[$index] = trim($email);
        }
        $main_email = $emails[0];
        unset($emails[0]);
        Mail::raw($text, function ($message) use ($emails, $main_email) {
            $message->to($main_email);
            $message->cc($emails);
            $message->subject('Новая заявка');
        });
        ShoppingCart::destroy();
        if (intval(Carbon::now()->timezone('Asia/Almaty')->format('H')) >= 10 && intval(Carbon::now()->timezone('Asia/Almaty')->format('H')) <= 22) {
            return redirect('/')->with('message', "Ваш номер заказа №$booking->id. Наш менеджер свяжется с Вами в течении 5 минут");
        } else {
            return redirect('/')->with('message', "Ваш номер заказа №$booking->id. Наш менеджер свяжется с Вами после 10:00");
        }
    }
}
