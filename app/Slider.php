<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'title',
        'order',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product', 'products_sliders');
    }

    public function images()
    {
        return $this->belongsToMany('App\Image', 'sliders_images', 'slider_id', 'image_id');
    }
}
