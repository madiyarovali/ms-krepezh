<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProductOption extends Model
{
    protected $fillable = [
        'product_id',
        'price_old',
        'option'
    ];

    public function getPriceAttribute()
    {
        $result = \DB::table('prices')->where('option_id', $this->id)->where('city_id', session('city_id', 1))->first();
        if ($result) {
            return $result->price;
        }
        return $this->price_old;
    }

    public function cities()
    {
        return $this
            ->belongsToMany('App\City', 'prices', 'option_id', 'city_id')
            ->withPivot([
                'price',
            ]);
    }
}
