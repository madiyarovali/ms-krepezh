$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
Notiflix.Notify.Init({
    distance: '30px'
});
$('#use-bonus').on('change', function () {
    $('.bonus').toggleClass('d-none');
    $('.total-li').toggleClass('d-none');
});
$('#form-delivery').on('change', function () {
    $('.total-li span').toggleClass('d-none');
});
$('[class^=trans]').on('click', function () {
    $('.col-product').toggleClass('col-md-4');
});
$('body').on('click', '.add_to_cart_btn', function (e) {
    e.preventDefault();
    let product_id = $(this).attr('data-product-id');
    let price = $(this).attr('data-product-price');
    let option_id = $(this).attr('data-product-option');
    let name = $(this).attr('data-product-name');
    let quantity = 1;
    if ($(this).siblings('.popular-product__btn-number').find('.count-product').length > 0) {
        quantity = $(this).siblings('.popular-product__btn-number').find('.count-product').val();
    }
    $.ajax({
        url: '/cart/add',
        method: 'POST',
        data: {
            name: name,
            price: price,
            option_id: option_id,
            quantity: quantity,
            attributes: [],
            product_id: product_id,
			
        },
        success: function (response) {
            $('.box-products-list').html(response.html);
            $('.box-products b').text(response.total);
            $('.btn-box p').text(response.count);
			Notiflix.Notify.Init({
                plainText: false
            });
            Notiflix.Notify.Success(' <a href="/checkout" class="text-underline">Оформить заказ</a>');
        }
    });
});

$('.delete-all a').click(function () {
    $('.order-box_goods li').each(function (index, element) {
        deleteProduct($(element).find('button'));
    });
});

$('body').on('click', '.box-products-list_close, .order-product_close', function (e) {
    e.preventDefault();
    deleteProduct(this);
});
var timer_cart;
$('body').on('click', '.control-btns', function (e) {
    e.preventDefault();
    let self = this;
    clearInterval(timer_cart);
    timer_cart = setTimeout(function () {
        $.ajax({
            url: '/cart/update',
            method: 'post',
            data: {
                row_id: $(self).attr('data-row-id'),
                quantity: $(self).siblings('.count-product').val()
            },
            success: function (response) {
                $('.box-products-list').html(response.html);
                $('.box-products b, .total').text(response.total);
                $('.count').text(response.count);
                Notiflix.Notify.Success('Количество изменено!');
            }
        });
    }, 2 * 1000);
});
$(document).ready(function () {
    let price = $('.vars').children("option:selected").attr('price');
    let option = $('.vars').children("option:selected").val();
    $('.add_to_cart_btn').attr('data-product-price', price);
    $('.add_to_cart_btn').attr('data-product-option', option);
    $('.product-characteristic-price').find('p').text(price);
});
$('.vars').on('change', function () {
    let price = $(this).children("option:selected").attr('price');
    let option = $(this).children("option:selected").val();
    $('.add_to_cart_btn').attr('data-product-price', price);
    $('.add_to_cart_btn').attr('data-product-option', option);
    $('.product-characteristic-price').find('p').text(price);
});
var timer;
$('.custom-checkbox input').on('change', function (e) {
    let self = this;
    updateProducts(e, timer, self);
});
$('a.order-by').on('click', function (e) {
    let self = this;
    $('.selected-sort').removeClass('selected-sort');
    $(self).addClass('selected-sort');
    updateProducts(e, timer, self);
});
function updateProducts (e, timer, self) {
    if ('preventDefault' in e) {
        e.preventDefault();
    }
    clearInterval(timer);
    if (!$(self).hasClass('js-range-slider')) {
        let filter = $(self).attr('data-filter');
        let option = $(self).text();

        $($(self).parents()[2]).find('.filter-title').text(option);
        $('.popular-products > .row').addClass('d-none');
        $('.loader').removeClass('d-none');
        // $($(self).parents()[1]).find('.marked-option').removeClass('marked-option');
        $(self).toggleClass('marked-option');
    }
    var from = ('from' in e) ? e.from : $('.js-range-slider').data("from");
    var to = ('to' in e) ? e.to : $('.js-range-slider').data("to"); 

    if (!$(self).hasClass('js-range-slider-length')) {
        let filter = $(self).attr('data-filter');
        let option = $(self).text();

        $($(self).parents()[2]).find('.filter-title').text(option);
        $('.popular-products > .row').addClass('d-none');
        $('.loader').removeClass('d-none');
        // $($(self).parents()[1]).find('.marked-option').removeClass('marked-option');
    }
    var from = ('from' in e) ? e.from : $('.js-range-slider').data("from");
    var to = ('to' in e) ? e.to : $('.js-range-slider').data("to"); 
    var dfrom = ('from' in e) ? e.from : $('.js-range-slider-length').data("from");
    var dto = ('to' in e) ? e.to : $('.js-range-slider-length').data("to"); 

    let page = $('.page-name').val();
    timer = setTimeout(function() { 
        var all_options = $('.marked-option').map(function(i, el) { return $(el).attr('data-option'); }).get();
        var all_filters = $('.marked-option').map(function(i, el) { return $(el).attr('data-filter'); }).get();
        var filters = [];
        var sortBy = $('.order-by.selected-sort').attr('data-key');
        var order = $('.order-by.selected-sort').attr('data-value');
        for (var i = all_filters.length - 1; i >= 0; i--) {
            filters.push({
                key: all_filters[i],
                val: all_options[i]
            });
        }
        console.log(filters);
        $.ajax({
            url: `/products/${page}`,
            data: { filters, sortBy, order, from, to, dfrom, dto },
            success: function (response) {
                $html = $(response);
                $html.closest('.row').remove();
                $('.popular-products > .row').html($html.html());
                $('.loader').addClass('d-none');
            }
        });
    }, 2 * 1000);
}
function deleteProduct (self) {
    let url = '/cart/remove';
    let row = $(self).attr('data-row');
    $.ajax({
        url: url,
        method: 'POST',
        data: {
            row_id: row
        },
        success: function (response) {
            $('.box-products-list').html(response.html);
            $($(`.box-products-list_close[data-row=${row}]`).parents()[0]).remove();
            $('.box-products b, .total').text(response.total);
            $('.count').text(response.count);
            Notiflix.Notify.Warning('Товар удален!');
        }
    });
}
$('.phone-mask').mask('+7 (000) 000 00 00');