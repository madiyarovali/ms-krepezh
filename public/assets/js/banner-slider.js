	jQuery("#carousel").owlCarousel({
  autoplay: true,
  lazyLoad: true,
  loop: true,
  margin: 20,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 10000,
  smartSpeed: 800,
  nav: true,
  responsive: {
    0: {
      items: 1
    },

    600: {
      items: 1
    },

    1024: {
      items: 1
    },

    1366: {
      items: 1
    }
  }
});


    jQuery("#recommend-slider").owlCarousel({
  autoplay: true,
  lazyLoad: true,
  loop: true,
  margin: 20,
    nav: true,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 5000,
  smartSpeed: 800,
   responsive: {
    0: {
      items: 1,
      dots:true,
    },

    600: {
      items: 2,
      dots:false,
    },

    1024: {
      items: 4,
      dots:false,
    },

    1366: {
      items: 4
    }
  }
});